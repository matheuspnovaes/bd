package controller;

import dao.DAOFactory;
import dao.EstatisticaDAO;
import dao.TaxistaDAO;
import dao.UsuarioDAO;
import dao.VeiculoDAO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Estatistica;
import model.Taxista;
import model.Usuario;
import model.Veiculo;

/**
 *
 * @author matheus
 */
@WebServlet(name = "EstatisticaController",
        urlPatterns = {
            "/estatistica",
            "/estatistica/quantidadeCorrida",
            "/estatistica/distanciaCorrida",
            "/estatistica/kmCorrida",
            "/estatistica/precoCorrida",
            "/estatistica/marcaVeiculo",
            "/estatistica/modeloVeiculo",
            "/estatistica/selectMarca",
            "/estatistica/selectModelo",
            "/estatistica/top10TaxistaCorrida", 
            "/estatistica/top10UsuarioPg",
            "/estatistica/top10TaxistaRb",
            "/estatistica/top10TaxistaNota",
            "/estatistica/top10UsuarioNota",
            "/estatistica/top10Cliente"
        })  
public class EstatisticaController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher;

        DAOFactory daoFactory;
        EstatisticaDAO estatisticaDAO;
        
        switch (request.getServletPath()) {
            case "/estatistica": {
                
                requestDispatcher = request.getRequestDispatcher("/view/estatistica/index.jsp");
                requestDispatcher.forward(request, response);
                
                break;
            }

            case "/estatistica/selectMarca": {
                
                requestDispatcher = request.getRequestDispatcher("/view/estatistica/selectMarca.jsp");
                requestDispatcher.forward(request, response);
                
                break;
            }
            case "/estatistica/selectModelo": {
                
                requestDispatcher = request.getRequestDispatcher("/view/estatistica/selectModelo.jsp");
                requestDispatcher.forward(request, response);
                
                break;
            }
            
            case "/estatistica/quantidadeCorrida" : {
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    int quantidade = estatisticaDAO.quantidadeCorrida();
                    
                    request.setAttribute("quantidade", quantidade);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/quantidadeCorrida.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            case "/estatistica/top10TaxistaCorrida":{
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    List<Estatistica> top10 = estatisticaDAO.top10TaxistaCorrida();
                    
                    request.setAttribute("top10", top10);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/top10TaxistaCorrida.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
            case "/estatistica/top10Cliente":{
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    List<Estatistica> top10 = estatisticaDAO.top10Cliente();
                    
                    request.setAttribute("top10", top10);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/top10Cliente.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            
             case "/estatistica/top10UsuarioNota":{
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    List<Estatistica> top10 = estatisticaDAO.top10UsuarioNota();
                    
                    request.setAttribute("top10", top10);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/top10UsuarioNota.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatistica/top10TaxistaNota":{
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    List<Estatistica> top10 = estatisticaDAO.top10TaxistaNota();
                    
                    request.setAttribute("top10", top10);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/top10TaxistaNota.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatistica/top10TaxistaRb":{
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    List<Estatistica> top10 = estatisticaDAO.top10TaxistaRb();
                    
                    request.setAttribute("top10", top10);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/top10TaxistaRb.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatistica/top10UsuarioPg":{
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    List<Estatistica> top10 = estatisticaDAO.top10UsuarioPg();
                    
                    request.setAttribute("top10", top10);
                    
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/top10UsuarioPg.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/estatistica/distanciaCorrida":{
            try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();

                    List<Estatistica> distanciasMin = estatisticaDAO.distanciaMin();
                    List<Estatistica> distanciasMax = estatisticaDAO.distanciaMax();
                    float mediaDistancia = estatisticaDAO.distanciaAVG();

                    request.setAttribute("distanciasMin", distanciasMin);
                    request.setAttribute("distanciasMax", distanciasMax);
                    request.setAttribute("mediaDistancia", mediaDistancia);

                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/distanciaCorrida.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
            
            case "/estatistica/kmCorrida":{
            try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();

                    List<Estatistica> kmsMin = estatisticaDAO.kmMin();
                    List<Estatistica> kmsMax = estatisticaDAO.kmMax();
                    float mediakm = estatisticaDAO.kmAVG();

                    request.setAttribute("kmsMin", kmsMin);
                    request.setAttribute("kmsMax", kmsMax);
                    request.setAttribute("mediakm", mediakm);

                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/kmCorrida.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
            case "/estatistica/precoCorrida":{
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();

                    List<Estatistica> precosMin = estatisticaDAO.precoMin();
                    List<Estatistica> precosMax = estatisticaDAO.precoMax();
                    float mediapreco = estatisticaDAO.precoAVG();

                    request.setAttribute("precosMin", precosMin);
                    request.setAttribute("precosMax", precosMax);
                    request.setAttribute("mediapreco", mediapreco);

                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/precoCorrida.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
        }

    }
    
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        DAOFactory daoFactory;
        EstatisticaDAO estatisticaDAO;
       
        switch (request.getServletPath()) {
            //finalizar 
            case "/estatistica/marcaVeiculo": {
                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    List<Estatistica> marcaVeiculos = estatisticaDAO.marcaVeiculo(request.getParameter("marca"));

                    request.setAttribute("marcaVeiculos", marcaVeiculos);
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/marcaVeiculo.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
            case "/estatistica/modeloVeiculo": {

                try {
                    daoFactory = new DAOFactory();
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    List<Estatistica> modeloVeiculos = estatisticaDAO.modeloVeiculo(request.getParameter("modelo"));

                    request.setAttribute("modeloVeiculos", modeloVeiculos);
                    requestDispatcher = request.getRequestDispatcher("/view/estatistica/modeloVeiculo.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EstatisticaController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }

            case "/veiculo/foto":{
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
     

}

