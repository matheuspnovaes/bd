package controller;

import dao.DAOFactory;
import dao.TaxistaDAO;
import dao.UsuarioDAO;
import dao.VeiculoDAO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Corrida;
import model.CorridaRequisicao;
import model.Rota;
import model.Taxista;
import model.ThreadPosicao;
import model.Usuario;
import model.Veiculo;

/**
 *
 * @author matheus
 */
@WebServlet(name = "TaxistaController",
        urlPatterns = {
            "/taxista",
            "/taxista/create",
            "/taxista/update",
            "/taxista/delete",
            "/taxista/chamadasCorrida",
            "/taxista/responderCliente",
            "/taxista/dadosCorrida",
            "/taxista/finalizar"
        })
public class TaxistaController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher;

        DAOFactory daoFactory;
        TaxistaDAO taxistaDAO;
        System.out.println("oi");
        switch (request.getServletPath()) {
            case "/taxista": {
                try {
                    daoFactory = new DAOFactory();
                    UsuarioDAO usuarioDAO = daoFactory.getUsuarioDAO();
                    VeiculoDAO veiculoDAO = daoFactory.getVeiculoDAO();
                    taxistaDAO = daoFactory.getTaxistaDAO();
                    HttpSession sessao = request.getSession(true);
                    Usuario usu = (Usuario) sessao.getAttribute("usuario");
                    Usuario usuario = usuarioDAO.read(usu.getId());
                    Taxista taxista = taxistaDAO.read(usu.getId());
                    Veiculo veiculo = veiculoDAO.dadosVeiculo(usu.getId());
                    System.out.println(veiculo.getAno());
                    
                    request.setAttribute("usuario", usuario);
                    request.setAttribute("veiculo", veiculo);
                    request.setAttribute("taxista", taxista);

                    requestDispatcher = request.getRequestDispatcher("/view/taxista/index.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break; 
            }

            case "/taxista/create": {
                requestDispatcher = request.getRequestDispatcher("/view/taxista/create.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            case "/taxista/finalizar": {
                requestDispatcher = request.getRequestDispatcher("/view/taxista/dadosCorrida.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

            case "/taxista/update": {
                try {
                    daoFactory = new DAOFactory();
                    taxistaDAO = daoFactory.getTaxistaDAO();

                    int id = Integer.parseInt(request.getParameter("id"));

                   Taxista taxista = taxistaDAO.read(id);

                    request.setAttribute("taxista", taxista);

                    requestDispatcher = request.getRequestDispatcher("/view/taxista/update.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(TaxistaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(TaxistaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/taxista/chamadasCorrida": {
                try {
                    daoFactory = new DAOFactory();

                    taxistaDAO = daoFactory.getTaxistaDAO();
                    HttpSession sessao = request.getSession(true);
               
                    Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                    List<CorridaRequisicao> corridasRq = taxistaDAO.chamadasCorrida(usuario.getId());
                    for(CorridaRequisicao cli : corridasRq){  
                        System.out.println(cli.getId_requisicao());  
                        System.out.println(cli.getStatus());  
                    } 

                    request.setAttribute("corridasRq", corridasRq);

                    requestDispatcher = request.getRequestDispatcher("/view/taxista/chamadasCorrida.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }
            
            case "/taxista/responderCliente": {
                try {
                    daoFactory = new DAOFactory();

                    taxistaDAO = daoFactory.getTaxistaDAO();
                    CorridaRequisicao corridaRq = new CorridaRequisicao();
                    HttpSession sessao = request.getSession(true);
                    Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                    corridaRq.setId_taxista(usuario.getId());
                    
                    corridaRq.setId_requisicao(Integer.parseInt(request.getParameter("id_rq")));
                    System.out.println(corridaRq.getId_requisicao());
                    corridaRq.setId_usuario(Integer.parseInt(request.getParameter("id")));
                    System.out.println(corridaRq.getId_usuario());
                    corridaRq.setStatus(Integer.parseInt(request.getParameter("i")));
                    System.out.println(corridaRq.getStatus());
                    
                    taxistaDAO.responderChamada(corridaRq);
                    List<Rota> pontos = taxistaDAO.pontos(usuario.getId());
                    CorridaRequisicao rq = taxistaDAO.latlng(Integer.parseInt(request.getParameter("id_rq")));
                    Thread t = new Thread(new ThreadPosicao(pontos,corridaRq.getId_requisicao(),rq.getLat(),rq.getLng()));
                    t.start();
                     response.sendRedirect(request.getContextPath() + "/taxista/chamadasCorrida");
                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }
            
            case "/taxista/dadosCorrida": {
                try {
                    daoFactory = new DAOFactory();
                    taxistaDAO = daoFactory.getTaxistaDAO();
                    UsuarioDAO usuarioDAO = daoFactory.getUsuarioDAO();
                   
                    HttpSession sessao = request.getSession(true);
               
                    //int id_rq =  (int) sessao.getAttribute("id_requisicao");
               
                    Corrida corrida = taxistaDAO.dadosCorrida(1);
                    
                    Usuario usuario = usuarioDAO.read(corrida.getId_usuario());
                    Usuario taxista = usuarioDAO.read(corrida.getId_taxista());
                    request.setAttribute("corrida", corrida);
                    request.setAttribute("usuario", usuario);
                    request.setAttribute("taxista", taxista);

                    requestDispatcher = request.getRequestDispatcher("/view/taxista/dadosCorrida.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }
            
        }

    }
    
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DAOFactory daoFactory;
        TaxistaDAO dao;
        VeiculoDAO daov;
        switch (request.getServletPath()) {
            case "/taxista/create": { 
               Taxista taxista = new Taxista();
               Veiculo veiculo  =  new Veiculo();
               HttpSession sessao = request.getSession(true);
               
               Usuario usuario = (Usuario) sessao.getAttribute("usuario");
               taxista.setId(usuario.getId());
               float valorkm = Float.parseFloat(request.getParameter("precokm"));
               taxista.setPrecoKm(valorkm);
               
               veiculo.setTaxistaID(taxista.getId());
               veiculo.setPlaca(request.getParameter("placa"));
               veiculo.setRenavam(request.getParameter("renavam"));
               veiculo.setMarca(request.getParameter("marca"));
               veiculo.setModelo(request.getParameter("modelo"));
               veiculo.setAno(request.getParameter("ano"));
               veiculo.setCor(request.getParameter("cor"));
               veiculo.setCapacidade(Integer.parseInt(request.getParameter("capacidade")));
               
                    sessao.setAttribute("veiculo", veiculo);
               try{

                    daoFactory = new DAOFactory();

                    dao = daoFactory.getTaxistaDAO();
                    
                    dao.create(taxista);
                    daov = daoFactory.getVeiculoDAO();
                    daov.create(veiculo);
                    response.sendRedirect(request.getContextPath() + "/veiculo/foto");

                
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/taxista/update": {

                Taxista taxista = new Taxista();
                float valorkm = Float.parseFloat(request.getParameter("precokm"));
               
                taxista.setPrecoKm(valorkm);
                taxista.setId(Integer.parseInt(request.getParameter("id")));
             

                try {
                    

                    daoFactory = new DAOFactory();

                    dao = daoFactory.getTaxistaDAO();

                    dao.update(taxista);

                    response.sendRedirect(request.getContextPath() + "/taxista");

                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
            case "/taxista/delete": {

                try { 
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getTaxistaDAO();
                    
                    int id = Integer.parseInt(request.getParameter("id"));
                    
                    dao.delete(id);                    
                    response.sendRedirect(request.getContextPath() +"/taxista");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(TaxistaController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(TaxistaController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            case "/taxista/dadosCorrida": {
                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getTaxistaDAO();
                  
                    HttpSession sessao = request.getSession(true);
               
                    //int id_rq =  (int) sessao.getAttribute("id_requisicao");
               
                   dao.validarCorrida(1, Integer.parseInt(request.getParameter("usuarionota")));

                    response.sendRedirect(request.getContextPath() + "/taxista");

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

