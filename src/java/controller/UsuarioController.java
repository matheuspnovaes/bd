/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.DAOFactory;
import dao.UsuarioDAO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Corrida;
import model.CorridaRequisicao;
import model.Rota;
import model.Taxista;
import model.Usuario;
import model.UsuarioPosition;






/**
 *
 * @author matheus
 */
@WebServlet(name = "UsuarioController",
        urlPatterns = {
            "/usuario",
            "/usuario/create",
            "/usuario/update",
            "/usuario/delete",
            "/usuario/buscarTaxi",
            "/usuario/chamarTaxi",
            "/usuario/statusCorrida",
            "/usuario/dadosCorrida",
            "/usuario/foto",
            "/usuario/pontos",
            "/usuario/historico",
            "/usuario/buscarRotas",
            "/usuario/rota",
            "/usuario/destino",
            "/usuario/finalizar"
        })
@MultipartConfig(maxFileSize = 16177215)  
public class UsuarioController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher;

        DAOFactory daoFactory;
        UsuarioDAO usuarioDao;
       
        switch (request.getServletPath()) {
            case "/usuario": {
                try {
                    daoFactory = new DAOFactory();

                   
                    usuarioDao = daoFactory.getUsuarioDAO();
                    HttpSession sessao = request.getSession(true);
                    Usuario usu = (Usuario) sessao.getAttribute("usuario");
                    Usuario usuario = usuarioDao.read(usu.getId());

                    request.setAttribute("usuario", usuario);

                    requestDispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }
            case "/usuario/buscarRotas": {

                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    Usuario usuario;
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    List<Rota> rotas;
                    HttpSession sessao = request.getSession(true);
                    UsuarioPosition usu = (UsuarioPosition) sessao.getAttribute("usuarioPosition");
                    rotas = usuarioDao.buscarTaxi(usu.getLatidude(),usu.getLongitude());
                    

                   GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                    Gson gson = builder.create();

                    // Preparar resposta JSON
                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");
                    PrintWriter out = response.getWriter();

                    // Conversão JSON e escrita da resposta
                    out.print(gson.toJson(rotas));
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            }
            case "/usuario/destino": {

                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
                    Usuario usuario;
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    Rota rotas;
                    HttpSession sessao = request.getSession(true);
                    int id_rq =  (int) sessao.getAttribute("id_requisicao");
                    rotas = usuarioDao.rotaDestino(id_rq);
                    
                    

                   GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                    Gson gson = builder.create();

                    // Preparar resposta JSON
                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");
                    PrintWriter out = response.getWriter();

                    // Conversão JSON e escrita da resposta
                    out.print(gson.toJson(rotas));
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            }
            
            case "/usuario/rota": {

                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();
             
                    
                    List<Rota> rota = usuarioDao.rota(Integer.parseInt(request.getParameter("id")));
                    
                    System.out.println("estou aqui");
                    for(Rota rotas: rota){
                        System.out.println(rotas.getIdRota());
                        System.out.println(rotas.getLatitude());
                    }
                   GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                    Gson gson = builder.create();

                    // Preparar resposta JSON
                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");
                    PrintWriter out = response.getWriter();

                    // Conversão JSON e escrita da resposta
                    out.print(gson.toJson(rota));
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            }

            case "/usuario/create": {
                requestDispatcher = request.getRequestDispatcher("/view/usuario/create.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
             case "/usuario/foto":{
                requestDispatcher = request.getRequestDispatcher("/view/usuario/foto.jsp");
                requestDispatcher.forward(request, response);
                break;
            }

            case "/usuario/update": {
                try {
                    daoFactory = new DAOFactory();
                    usuarioDao = daoFactory.getUsuarioDAO();

                    int id = Integer.parseInt(request.getParameter("id"));

                    Usuario usuario = usuarioDao.read(id);

                    request.setAttribute("usuario", usuario);

                    requestDispatcher = request.getRequestDispatcher("/view/usuario/update.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
           
            case "/usuario/buscarTaxi": {
                requestDispatcher = request.getRequestDispatcher("/view/usuario/buscarTaxi.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
            case "/usuario/statusCorrida": {
                  try {
                    daoFactory = new DAOFactory();

                    usuarioDao = daoFactory.getUsuarioDAO();
                    HttpSession sessao = request.getSession(true);
               
                    int id_rq =  (int) sessao.getAttribute("id_requisicao");
               
                    List<CorridaRequisicao> corridasRq = usuarioDao.statusCorrida(id_rq);
                    
                   
                    
                    
                    request.setAttribute("corridasRq", corridasRq);

                   requestDispatcher = request.getRequestDispatcher("/view/usuario/statusCorrida.jsp");
                   requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                break;
            }
            case "/usuario/historico":{
                try {
                    daoFactory = new DAOFactory();

                    usuarioDao = daoFactory.getUsuarioDAO();
                    HttpSession sessao = request.getSession(true);
                    Usuario usuario = (Usuario) sessao.getAttribute("usuario");

                    List<Corrida> corridas = usuarioDao.historicoCorrida(usuario.getId());
                     
                    
                    for(Corrida corrida:corridas){
                        System.out.println(corrida.getNota_taxista());
                    }

                    request.setAttribute("corridas", corridas);

                    requestDispatcher = request.getRequestDispatcher("/view/usuario/historico.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
            case "/usuario/pontos": {
                try {
                    daoFactory = new DAOFactory();

                    usuarioDao = daoFactory.getUsuarioDAO();
                     HttpSession sessao = request.getSession(true);
               
                    int id_rq =  (int) sessao.getAttribute("id_requisicao");
               
                    List<CorridaRequisicao> corridasRq = usuarioDao.statusCorrida(id_rq);
                    GsonBuilder builder = new GsonBuilder();
                    builder.setDateFormat("dd-MM-yyyy hh:mm:ss.S");
                    Gson gson = builder.create();

                    // Preparar resposta JSON
                    response.setContentType("application/json");
                    response.setCharacterEncoding("utf-8");
                    PrintWriter out = response.getWriter();

                    // Conversão JSON e escrita da resposta
                    out.print(gson.toJson(corridasRq));
                   
                    
                    
                    //request.setAttribute("corridasRq", corridasRq);

                   //requestDispatcher = request.getRequestDispatcher("/view/usuario/statusCorrida.jsp");
                  // requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }
            
            
            case "/usuario/chamarTaxi": {

                try {
                    daoFactory = new DAOFactory();
                    UsuarioDAO dao = daoFactory.getUsuarioDAO();
                    
                    HttpSession sessao = request.getSession(true);
                    Usuario usuario = (Usuario) sessao.getAttribute("usuario");
                    UsuarioPosition usuarioPosition = (UsuarioPosition) sessao.getAttribute("usuarioPosition");
                   int id = Integer.parseInt(request.getParameter("id"));
                   double lat = Double.parseDouble(request.getParameter("lat"));
                   double lng = Double.parseDouble(request.getParameter("lng"));
                   System.out.println("taxi "+id);
                   System.out.println("usuario "+usuario.getId());
                   System.out.println("posicao "+usuarioPosition.getLatidude());
                   int id_requisicao = dao.requisicaoTaxi(id,usuario.getId(), usuarioPosition.getLatidude(), usuarioPosition.getLongitude(),lat,lng);
                   sessao.setAttribute("id_requisicao",id_requisicao);
                  
                    sessao.setAttribute("id_requisicao", id_requisicao); 
                    response.sendRedirect(request.getContextPath() + "/usuario/statusCorrida");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            }

            
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher;
        DAOFactory daoFactory;
        UsuarioDAO dao;
        switch (request.getServletPath()) {
            case "/usuario/create": {
                Usuario usuario = new Usuario();

                usuario.setNome(request.getParameter("nome"));
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));
                usuario.setCpf(request.getParameter("cpf"));
                usuario.setTelefone(request.getParameter("telefone"));
                usuario.setSexo(request.getParameter("sexo"));
                
                /*final Part filePart = request.getPart("foto");
                final String fileName = getFileName(filePart);
                usuario.setFoto(fileName);
                final String path = "/assets/img/";
                OutputStream out = null;
                InputStream filecontent = null;
                 
                try {
                    out = new FileOutputStream(new File(path + File.separator + fileName));
                    filecontent = filePart.getInputStream();

                    int read = 0;
                    final byte[] bytes = new byte[1024];

                    while ((read = filecontent.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                } catch (FileNotFoundException fne) {
                    System.err.println(fne.getMessage());
                 } finally {
                    if (out != null) {
                         out.close();
                    }
                    if (filecontent != null) {
                        filecontent.close();
                    }
        
                }*/
                
                

                      
                            //File uploadedFile = new File("/assets/img/"  + item.getName() +"_"+ new Date().getTime() );
                            
            

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                try {
                    Date dataNascimento = dateFormat.parse(request.getParameter("nascimento"));

                    usuario.setDataNascimento(new java.sql.Date(dataNascimento.getTime()));

                    daoFactory = new DAOFactory();

                    dao = daoFactory.getUsuarioDAO();

                    dao.create(usuario);

                    response.sendRedirect(request.getContextPath() + "/login");

                } catch (ParseException ex) {
                    System.err.println(ex.getMessage());
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/usuario/update": {

                Usuario usuario = new Usuario();

                usuario.setId(Integer.parseInt(request.getParameter("id")));
                usuario.setNome(request.getParameter("nome"));
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                try {
                    Date dataNascimento = dateFormat.parse(request.getParameter("datanasc"));

                    usuario.setDataNascimento(new java.sql.Date(dataNascimento.getTime()));

                    daoFactory = new DAOFactory();

                    dao = daoFactory.getUsuarioDAO();

                    dao.update(usuario);

                    response.sendRedirect(request.getContextPath() + "/usuario");

                } catch (ParseException ex) {
                    System.err.println(ex.getMessage());
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
            
            case "/usuario/delete": {

                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getUsuarioDAO();
                    
                    int id = Integer.parseInt(request.getParameter("id"));
                    
                    dao.delete(id);                    
                    
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            }
            
             case "/usuario/buscarTaxi": {

                try {
                    daoFactory = new DAOFactory();
                    dao = daoFactory.getUsuarioDAO();
                    Usuario usuario;
                    List<Usuario> usuarios = new ArrayList<Usuario>();
                    List<Rota> rotas; 
                    rotas = dao.buscarTaxi(Double.parseDouble(request.getParameter("latitude")),Double.parseDouble(request.getParameter("longitude")));
                    UsuarioPosition usuarioPosition = new UsuarioPosition();
                    usuarioPosition.setLatidude(Double.parseDouble(request.getParameter("latitude")));
                    usuarioPosition.setLongitude(Double.parseDouble(request.getParameter("longitude")));
                    HttpSession sessao = request.getSession(true);
                    sessao.setAttribute("usuarioPosition", usuarioPosition);

                    int i;
                    for(i = 0; i < rotas.size(); i++){
                        usuario = new Usuario();
                        usuario = dao.read(rotas.get(i).getIdTaxista());
                        usuarios.add(usuario);
                        
                }
                    request.setAttribute("rotas", rotas);

                    requestDispatcher = request.getRequestDispatcher("/view/usuario/chamarTaxi.jsp");
                    requestDispatcher.forward(request, response);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            }
            
             case "/usuario/dadosCorrida": {
                try {
                    daoFactory = new DAOFactory();

                    UsuarioDAO usuarioDao = daoFactory.getUsuarioDAO();
                    HttpSession sessao = request.getSession(true);
               
                    //int id_rq =  (int) sessao.getAttribute("id_requisicao");
               
                    usuarioDao.validarCorrida(1, Integer.parseInt(request.getParameter("taxistanota")));

                    response.sendRedirect(request.getContextPath() + "/usuario");

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }
             case "/usuario/finalizar": {
                try {
                    daoFactory = new DAOFactory();

                    UsuarioDAO usuarioDao = daoFactory.getUsuarioDAO();
                    HttpSession sessao = request.getSession(true);
               
                    //int id_rq =  (int) sessao.getAttribute("id_requisicao");
               
                    Corrida corrida = usuarioDao.dadosCorrida(1);
                    
                    Usuario usuario = usuarioDao.read(corrida.getId_usuario());
                    Usuario taxista = usuarioDao.read(corrida.getId_taxista());
                    request.setAttribute("corrida", corrida);
                    request.setAttribute("usuario", usuario);
                    request.setAttribute("taxista", taxista);

                    requestDispatcher = request.getRequestDispatcher("/view/usuario/dadosCorrida.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }
             case "/usuario/foto":{
                HttpSession sessao = request.getSession(true);
               
               Usuario usuario = (Usuario) sessao.getAttribute("usuario");
               System.out.println(usuario.getId());
               final Part filePart = request.getPart("foto");
                final String fileName = getFileName(filePart);
                String appPath = request.getServletContext().getRealPath("");
                String fotoNome = fileName+"_"+ new Date().getTime();
                usuario.setFoto(fotoNome);
                String path = appPath + File.separator +"assets/img/"+fotoNome;
                System.out.println(path);
                
                // usuario.setFoto(path + File.separator + fileName +"_"+ new Date().getTime());
                OutputStream out = null;
                InputStream filecontent = null;
                 
                try {
                    out = new FileOutputStream(new File(path));
                    filecontent = filePart.getInputStream();

                    int read = 0;
                    final byte[] bytes = new byte[1024];

                    while ((read = filecontent.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    daoFactory = new DAOFactory();

                    dao = daoFactory.getUsuarioDAO();

                    dao.uploadFoto(usuario);

                    response.sendRedirect(request.getContextPath() + "/login");
                } catch (FileNotFoundException fne) {
                    System.err.println(fne.getMessage());
                 } catch (ClassNotFoundException ex) {
                Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    if (out != null) {
                         out.close();
                    }
                    if (filecontent != null) {
                        filecontent.close();
                    }
        
                }
               break;
            }
            
            

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}
