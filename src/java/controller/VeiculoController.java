package controller;

import dao.DAOFactory;
import dao.TaxistaDAO;
import dao.UsuarioDAO;
import dao.VeiculoDAO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Taxista;
import model.Usuario;
import model.Veiculo;

/**
 *
 * @author matheus
 */
@WebServlet(name = "VeiculoController",
        urlPatterns = {
            "/veiculo",
            "/veiculo/update",
            "/veiculo/delete",
            "/veiculo/foto"
        })
@MultipartConfig(maxFileSize = 16177215)  
public class VeiculoController extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher;

        DAOFactory daoFactory;
        VeiculoDAO veiculoDAO;
        switch (request.getServletPath()) {
            case "/veiculo": {
                try {
                    daoFactory = new DAOFactory();

                    veiculoDAO = daoFactory.getVeiculoDAO();

                    List<Veiculo> veiculos = veiculoDAO.all();

                    request.setAttribute("veiculos", veiculos);

                    requestDispatcher = request.getRequestDispatcher("/view/veiculo/index.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException | SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                break;
            }

            

            case "/veiculo/update": {
                try {
                    daoFactory = new DAOFactory();
                    veiculoDAO = daoFactory.getVeiculoDAO();

                    int id = Integer.parseInt(request.getParameter("id"));

                   Veiculo veiculo = veiculoDAO.read(id);

                    request.setAttribute("veiculo", veiculo);

                    requestDispatcher = request.getRequestDispatcher("/view/veiculo/update.jsp");
                    requestDispatcher.forward(request, response);

                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
            case "/veiculo/foto":{
                requestDispatcher = request.getRequestDispatcher("/view/veiculo/foto.jsp");
                requestDispatcher.forward(request, response);
                break;
            }
            
        }

    }
    
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        DAOFactory daoFactory;
        TaxistaDAO dao;
        VeiculoDAO daov;
        switch (request.getServletPath()) {
            
            case "/veiculo/update": {

                Veiculo veiculo = new Veiculo();
               
                veiculo.setPlaca(request.getParameter("placa"));
                veiculo.setRenavam(request.getParameter("renavam"));
                veiculo.setMarca(request.getParameter("marca"));
                veiculo.setModelo(request.getParameter("modelo"));
                veiculo.setAno(request.getParameter("ano"));
                veiculo.setCor(request.getParameter("cor"));
                veiculo.setCapacidade(Integer.parseInt(request.getParameter("capacidade")));
             

                try {
                    

                    daoFactory = new DAOFactory();

                    daov = daoFactory.getVeiculoDAO();

                    daov.update(veiculo);

                    response.sendRedirect(request.getContextPath() + "/veiculo");

                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;
            }
            case "/veiculo/delete": {

                try { 
                    daoFactory = new DAOFactory();
                    daov = daoFactory.getVeiculoDAO();
                    
                    int id = Integer.parseInt(request.getParameter("id"));
                    
                    daov.delete(id);                    
                    response.sendRedirect(request.getContextPath() +"/veiculo");
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(VeiculoController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            
            case "/veiculo/foto":{
                HttpSession sessao = request.getSession(true);
               
               Veiculo veiculo = (Veiculo) sessao.getAttribute("veiculo");
               System.out.println(veiculo.getId());
               final Part filePart = request.getPart("foto");
                final String fileName = getFileName(filePart);
                final String path = "/home/matheus/Documentos/bd2015/web/assets/img"; 
                veiculo.setFoto(path + File.separator + fileName +"_"+ new Date().getTime());
                OutputStream out = null;
                InputStream filecontent = null;
                 
                try {
                    out = new FileOutputStream(new File(veiculo.getFoto()));
                    filecontent = filePart.getInputStream();

                    int read = 0;
                    final byte[] bytes = new byte[1024];

                    while ((read = filecontent.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    daoFactory = new DAOFactory();

                    daov = daoFactory.getVeiculoDAO();

                    daov.uploadFoto(veiculo);

                    response.sendRedirect(request.getContextPath() + "/login");
                } catch (FileNotFoundException fne) {
                    System.err.println(fne.getMessage());
                 } catch (ClassNotFoundException ex) {
                Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(UsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    if (out != null) {
                         out.close();
                    }
                    if (filecontent != null) {
                        filecontent.close();
                    }
        
                }
               
                
            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
     private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

}

