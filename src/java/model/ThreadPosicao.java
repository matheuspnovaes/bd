/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dao.DAOFactory;
import dao.TaxistaDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matheus
 */
public class ThreadPosicao implements Runnable {
    private List<Rota> pontos;
    private int id_requesicao;
    private double lat;
    private double lng;
    public ThreadPosicao(List<Rota> pontos, int id_requisicao, double lat, double lng){
        this.pontos = pontos;
        this.id_requesicao = id_requisicao;
        this.lat = lat;
        this.lng = lng;
    }
    @Override
    public void run() {
        for(Rota ponto: pontos){
            DAOFactory daoFactory;
            try {
                daoFactory = new DAOFactory();
                TaxistaDAO taxistaDAO = daoFactory.getTaxistaDAO();
                System.out.println(this.lat);
                System.out.println(this.lng);
                System.out.println(ponto.getLatitude());
                System.out.println(ponto.getLongitude());
                if(this.lat != ponto.getLatitude() && this.lng != ponto.getLongitude()){
                    taxistaDAO.mandarPontos(ponto.getLatitude(), ponto.getLongitude(), this.id_requesicao);
                    Thread.sleep(5000);
                }else{
                    System.out.println("Teste Thread");
                    taxistaDAO.udpdateStatus(this.id_requesicao, 3,ponto.getIdRota());
                    break;
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ThreadPosicao.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ThreadPosicao.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ThreadPosicao.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadPosicao.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
}
