/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author matheus
 */
public class Taxista extends Usuario{
    private int id;
    private float precoKm;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the precoKm
     */
    public float getPrecoKm() {
        return precoKm;
    }

    /**
     * @param precoKm the precoKm to set
     */
    public void setPrecoKm(float precoKm) {
        this.precoKm = precoKm;
    }
}
