/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author matheus
 */
public class UsuarioPosition {
    private int id;
    private double latidude;
    private double longitude;
    private Timestamp observ;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the latidude
     */
    public double getLatidude() {
        return latidude;
    }

    /**
     * @param latidude the latidude to set
     */
    public void setLatidude(double latidude) {
        this.latidude = latidude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the observ
     */
    public Timestamp getObserv() {
        return observ;
    }

    /**
     * @param observ the observ to set
     */
    public void setObserv(Timestamp observ) {
        this.observ = observ;
    }
}
