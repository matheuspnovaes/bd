/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author matheus
 */
public class Estatistica {
    private int idTaxista;
    private int idUsuario;
    private int quantidadeCorrida;
    private int notaTaxista;
    private int notaUsuario;
    private double distanciaCorrida;
    private float precoCorrida;
    private float valorKm;
    private String marca;
    private String modelo;
    private String placa;
    private String taxistaNome;
    private String usuarioNome;
    private float mediaNota;

    /**
     * @return the idTaxista
     */
    public int getIdTaxista() {
        return idTaxista;
    }

    /**
     * @param idTaxista the idTaxista to set
     */
    public void setIdTaxista(int idTaxista) {
        this.idTaxista = idTaxista;
    }

    /**
     * @return the idUsuario
     */
    public int getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the quantidadeCorrida
     */
    public int getQuantidadeCorrida() {
        return quantidadeCorrida;
    }

    /**
     * @param quantidadeCorrida the quantidadeCorrida to set
     */
    public void setQuantidadeCorrida(int quantidadeCorrida) {
        this.quantidadeCorrida = quantidadeCorrida;
    }

    /**
     * @return the notaTaxista
     */
    public int getNotaTaxista() {
        return notaTaxista;
    }

    /**
     * @param notaTaxista the notaTaxista to set
     */
    public void setNotaTaxista(int notaTaxista) {
        this.notaTaxista = notaTaxista;
    }

    /**
     * @return the notaUsuario
     */
    public int getNotaUsuario() {
        return notaUsuario;
    }

    /**
     * @param notaUsuario the notaUsuario to set
     */
    public void setNotaUsuario(int notaUsuario) {
        this.notaUsuario = notaUsuario;
    }

    /**
     * @return the distanciaCorrida
     */
    public double getDistanciaCorrida() {
        return distanciaCorrida;
    }

    /**
     * @param distanciaCorrida the distanciaCorrida to set
     */
    public void setDistanciaCorrida(double distanciaCorrida) {
        this.distanciaCorrida = distanciaCorrida;
    }

    /**
     * @return the precoCorrida
     */
    public float getPrecoCorrida() {
        return precoCorrida;
    }

    /**
     * @param precoCorrida the precoCorrida to set
     */
    public void setPrecoCorrida(float precoCorrida) {
        this.precoCorrida = precoCorrida;
    }

    /**
     * @return the valorKm
     */
    public float getValorKm() {
        return valorKm;
    }

    /**
     * @param valorKm the valorKm to set
     */
    public void setValorKm(float valorKm) {
        this.valorKm = valorKm;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the taxistaNome
     */
    public String getTaxistaNome() {
        return taxistaNome;
    }

    /**
     * @param taxistaNome the taxistaNome to set
     */
    public void setTaxistaNome(String taxistaNome) {
        this.taxistaNome = taxistaNome;
    }

    /**
     * @return the usuarioNome
     */
    public String getUsuarioNome() {
        return usuarioNome;
    }

    /**
     * @param usuarioNome the usuarioNome to set
     */
    public void setUsuarioNome(String usuarioNome) {
        this.usuarioNome = usuarioNome;
    }

    /**
     * @return the mediaNota
     */
    public float getMediaNota() {
        return mediaNota;
    }

    /**
     * @param mediaNota the mediaNota to set
     */
    public void setMediaNota(float mediaNota) {
        this.mediaNota = mediaNota;
    }
}
