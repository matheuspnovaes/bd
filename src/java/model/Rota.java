/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author matheus
 */
public class Rota{
    private int idRota;
    private int idTaxista;
    private double latitude;
    private double longitude;
    private Timestamp observ;

    /**
     * @return the idRota
     */
    public int getIdRota() {
        return idRota;
    }

    /**
     * @param idRota the idRota to set
     */
    public void setIdRota(int idRota) {
        this.idRota = idRota;
    }

    /**
     * @return the idTaxista
     */
    public int getIdTaxista() {
        return idTaxista;
    }

    /**
     * @param idTaxista the idTaxista to set
     */
    public void setIdTaxista(int idTaxista) {
        this.idTaxista = idTaxista;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the observ
     */
    public Timestamp getObserv() {
        return observ;
    }

    /**
     * @param observ the observ to set
     */
    public void setObserv(Timestamp observ) {
        this.observ = observ;
    }
    
}
