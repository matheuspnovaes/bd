/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author matheus
 */
public class Corrida {
    private int id;
    private int id_requisicao;
    private int id_usuario;
    private int id_taxista;
    private int nota_taxista;
    private int nota_usuario;
    private double distancia_corrida;
    private float valor_corrida;
    private float valorkm;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_requisicao
     */
    public int getId_requisicao() {
        return id_requisicao;
    }

    /**
     * @param id_requisicao the id_requisicao to set
     */
    public void setId_requisicao(int id_requisicao) {
        this.id_requisicao = id_requisicao;
    }

    /**
     * @return the id_usuario
     */
    public int getId_usuario() {
        return id_usuario;
    }

    /**
     * @param id_usuario the id_usuario to set
     */
    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    /**
     * @return the id_taxsita
     */
    public int getId_taxista() {
        return id_taxista;
    }

    /**
     * @param id_taxsita the id_taxsita to set
     */
    public void setId_taxista(int id_taxsita) {
        this.id_taxista = id_taxsita;
    }

    /**
     * @return the nota_taxista
     */
    public int getNota_taxista() {
        return nota_taxista;
    }

    /**
     * @param nota_taxista the nota_taxista to set
     */
    public void setNota_taxista(int nota_taxista) {
        this.nota_taxista = nota_taxista;
    }

    /**
     * @return the nota_usuario
     */
    public int getNota_usuario() {
        return nota_usuario;
    }

    /**
     * @param nota_usuario the nota_usuario to set
     */
    public void setNota_usuario(int nota_usuario) {
        this.nota_usuario = nota_usuario;
    }

    /**
     * @return the distancia_corrida
     */
    public double getDistancia_corrida() {
        return distancia_corrida;
    }

    /**
     * @param distancia_corrida the distancia_corrida to set
     */
    public void setDistancia_corrida(double distancia_corrida) {
        this.distancia_corrida = distancia_corrida;
    }

    /**
     * @return the valor_corrida
     */
    public float getValor_corrida() {
        return valor_corrida;
    }

    /**
     * @param valor_corrida the valor_corrida to set
     */
    public void setValor_corrida(float valor_corrida) {
        this.valor_corrida = valor_corrida;
    }

    /**
     * @return the valorkm
     */
    public float getValorkm() {
        return valorkm;
    }

    /**
     * @param valorkm the valorkm to set
     */
    public void setValorkm(float valorkm) {
        this.valorkm = valorkm;
    }
}
