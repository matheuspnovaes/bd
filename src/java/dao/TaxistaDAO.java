/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Corrida;
import model.CorridaRequisicao;
import model.Rota;
import model.Taxista;
import model.Usuario;

/**
 *
 * @author pedro
 */
public class TaxistaDAO extends DAO<Taxista> {
    
    private final String createQuery = "INSERT INTO taxista(id, valor_km) "
            + "VALUES (?, ?) RETURNING id;";
    
    /*private final String allQuery = "SELECT u.id, u.nome FROM taxista t, usuario u"
                                            + "WHERE t.id = u.id";*/
    private final String allQuery = "SELECT id, valor_km FROM taxista";
    
    private final String readQuery = "SELECT * FROM taxista WHERE id = ?;";
    
    private final String updateQuery = "UPDATE taxista "
                                     + "SET valor_km = ?"
                                     + "WHERE id = ?;";
    
    private final String deleteQuery  = "DELETE FROM taxista WHERE id = ?;";
    
    private final String chamadaCorrida = "SELECT * FROM requisicao_corrida "
                            + "WHERE id_taxista = ? ;";
    private final String responderChamada = "UPDATE requisicao_corrida "
                                            +"SET status = ? "
                                            + "WHERE id_taxista = ? AND id_usuario = ? AND id = ? ;";
    private final String updateStatus = "UPDATE requisicao_corrida SET status = ? , id_rota = ? WHERE id = ?;";
    private final String mandarPontosQuery ="UPDATE requisicao_corrida SET latitude = ?, longitude = ? WHERE id = ?;";
    private final String dadosCorridaQuery = "SELECT * FROM corrida WHERE id_requisicao = ?;";
    private final String validarCorridaQuery = "UPDATE corrida SET nota_usuario = ? WHERE id_requisicao = ?;";
    private final String pontosQuery = "SELECT * FROM pontos p, rotas r  "
                                            +"WHERE r.id_taxista = ? AND r.id = p.id_rotas;";
    private final String latLngQuery = "SELECT lat, lng FROM requisicao_corrida WHERE id = ?";
    public TaxistaDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Taxista taxista) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setInt(1, taxista.getId());
            statement.setFloat(2,taxista.getPrecoKm());
            
            
            ResultSet result = statement.executeQuery();
            
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Taxista read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Taxista taxista = new Taxista();
            
            if (result.next()) {
                taxista.setId(result.getInt("id"));
                taxista.setPrecoKm(result.getFloat("valor_km"));
                
            }
            
            return taxista;
            
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return null;
        
    }

    @Override
    public void update(Taxista taxista) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setFloat(1,taxista.getPrecoKm());
            statement.setInt(2, taxista.getId());
    
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Taxista> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Taxista> taxistas = new ArrayList<Taxista>();
            
            Taxista taxista;
            
            while (result.next()) {
                taxista = new Taxista();
                
                taxista.setId(result.getInt("id"));
                taxista.setPrecoKm(result.getFloat("valor_km"));
                
                taxistas.add(taxista);
            }
            
            return taxistas;
            
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public List<CorridaRequisicao> chamadasCorrida(int id_taxista){
        try {
            PreparedStatement statement = connection.prepareStatement(chamadaCorrida);
            
            statement.setInt(1, id_taxista);
            ResultSet result = statement.executeQuery();
            
            List<CorridaRequisicao> corridasRq = new ArrayList<CorridaRequisicao>();
            CorridaRequisicao corridaRq;
            
            while(result.next()){
                corridaRq = new CorridaRequisicao();
                corridaRq.setId_requisicao(result.getInt("id"));
                corridaRq.setId_taxista(result.getInt("id_taxista"));
                corridaRq.setId_usuario(result.getInt("id_usuario"));
                corridaRq.setLatitude(result.getDouble("latitude"));
                corridaRq.setLongitude(result.getDouble("longitude"));
                corridaRq.setStatus(result.getInt("status"));
                corridasRq.add(corridaRq);
            }
            return corridasRq;
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void responderChamada(CorridaRequisicao corridaRq){
        try {
            PreparedStatement statement = connection.prepareStatement(responderChamada);
            
            statement.setInt(1, corridaRq.getStatus());
            statement.setInt(2, corridaRq.getId_taxista());
            statement.setInt(3, corridaRq.getId_usuario());
            statement.setInt(4,corridaRq.getId_requisicao());
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public Corrida dadosCorrida(int id_requisição){
        try {
            PreparedStatement statement = connection.prepareStatement(dadosCorridaQuery);
            
            statement.setInt(1, id_requisição);
            
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            while(result.next()){
                corrida.setId(result.getInt("id"));
                corrida.setDistancia_corrida(result.getDouble("distancia_corrida"));
                corrida.setId_requisicao(result.getInt("id_requisicao"));
                corrida.setId_taxista(result.getInt("id_taxista"));
                corrida.setId_usuario(result.getInt("id_usuario"));
                corrida.setValor_corrida(result.getFloat("preco_corrida"));
                corrida.setValorkm(result.getFloat("valorkm"));
            }
            return corrida;
            } catch (SQLException ex) {
                 Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
     public void validarCorrida(int id_requisicao, int nota_usuario){
        try {
            PreparedStatement statement = connection.prepareStatement(validarCorridaQuery);
            statement.setInt(1, nota_usuario);
            statement.setInt(2, id_requisicao);
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void mandarPontos(double latitude, double longitude, int id_requisicao){
        try {
            PreparedStatement statement = connection.prepareStatement(mandarPontosQuery);
            statement.setDouble(1, latitude);
            statement.setDouble(2, longitude);
            statement.setInt(3, id_requisicao);
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Rota> pontos (int id_taxista){
        try {
            PreparedStatement statement = connection.prepareStatement(pontosQuery);
            statement.setInt(1, id_taxista);
            ResultSet result = statement.executeQuery();
            List<Rota> pontos = new ArrayList<Rota>();
            Rota rota;
            while(result.next()){
                rota = new Rota();
                rota.setIdRota(result.getInt("id_rotas"));
                rota.setIdTaxista(result.getInt("id_taxista"));
                rota.setLatitude(result.getDouble("latitude"));
                rota.setLongitude(result.getDouble("longitude"));
                pontos.add(rota);
            }
            return pontos;
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public void udpdateStatus(int id, int status, int id_rota){
        try {
            PreparedStatement statement = connection.prepareStatement(updateStatus);
            statement.setInt(1,status);
            statement.setInt(2,id_rota);
            statement.setInt(3, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public CorridaRequisicao latlng(int id){
        try {
            PreparedStatement statement = connection.prepareStatement(latLngQuery);
            statement.setInt(1,id);
            ResultSet result = statement.executeQuery();
            
            CorridaRequisicao rq = new CorridaRequisicao();
            if(result.next()){
                rq.setLat(result.getDouble("lat"));
                rq.setLng(result.getDouble("lng"));
            }
            return rq;
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
