/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Estatistica;

/**
 *
 * @author matheus
 */
public class EstatisticaDAO extends DAO<Estatistica>{
    
    private final String corridaQuantidadeQuerry = "SELECT  count(id) AS quantidade FROM corrida;";
    
    private final String distanciaMinQuerry = "SELECT  * FROM corrida"+
                  " WHERE distancia_corrida = (SELECT min(distancia_corrida) FROM corrida);";
    
    private final String distanciaMaxQuerry = "SELECT  * FROM corrida"+
                  " WHERE distancia_corrida = (SELECT max(distancia_corrida) FROM corrida);";
    
    private final String distanciaAVGQuerry = "SELECT avg(distancia_corrida) AS media FROM corrida;";
    
    private final String kmMinQuerry = "SELECT  * FROM corrida"+
                    " WHERE valorkm = (SELECT min(valorkm) FROM corrida);";
    
    private final String kmMaxQuerry = "SELECT  * FROM corrida"+
                    " WHERE valorkm = (SELECT max(valorkm) FROM corrida);";
    
    private final String kmAVGQuerry = "SELECT avg(valorkm) as media FROM corrida;";
    
    private final String precoMinQuerry = "SELECT  * FROM corrida" +
                      "	WHERE preco_corrida = (SELECT min(preco_corrida) FROM corrida);";
    
    private final String precoMaxQuerry = "SELECT  * FROM corrida" +
                      "	WHERE preco_corrida = (SELECT max(preco_corrida) FROM corrida);";
    
    private final String precoAVGQuerry = "SELECT avg(preco_corrida) as media  FROM corrida;";
    
    private final String marcaVeiculoQuerry = "SELECT * FROM veiculo v, corrida c" +
                    " WHERE v.taxista_id = c.id_taxista AND v.marca = ?;";
    
    private final String modeloVeiculoQuerry = "SELECT * FROM veiculo v, corrida c" +
                    " WHERE v.taxista_id = c.id_taxista AND v.modelo = ?;";
    
    private final String top10TaxistaCorridaQuerry = "SELECT nome, count AS quantidade FROM(SELECT id_taxista,  count(*) FROM corrida GROUP BY id_taxista) t, usuario u"+
                        " WHERE t.id_taxista = u.id LIMIT 10;";
    
    private final String top10UsuarioPgQuerry = "SELECT  * FROM (SELECT  DISTINCT c.id_usuario FROM (SELECT * FROM corrida ORDER BY preco_corrida DESC)AS c)AS top, usuario c" +
                            " WHERE top.id_usuario = c. id LIMIT 10;";
     private final String top10TaxistaRbQuerry = "SELECT  * FROM (SELECT  DISTINCT c.id_taxista FROM (SELECT * FROM corrida ORDER BY preco_corrida DESC)AS c)AS top, usuario c" +
                            " WHERE top.id_taxista = c.id LIMIT 10;";
     private final String top10TaxistaNotaQuerry ="SELECT nome, avg AS media FROM(SELECT * FROM(SELECT id_taxista, avg(nota_usuario) FROM corrida GROUP BY id_taxista)AS a ORDER BY avg DESC) AS t, usuario u"+
                    " WHERE t.id_taxista = u.id LIMIT 10;";
     private final String top10UsuarioNotaQuerry ="SELECT nome, avg AS media FROM(SELECT * FROM(SELECT id_usuario, avg(nota_taxista) FROM corrida GROUP BY id_usuario)AS a ORDER BY avg DESC) AS t, usuario u"+
                    " WHERE t.id_usuario = u.id LIMIT 10;";
    
     private final String top10ClienteQuerry = "SELECT COUNT(*) AS quantidade, id_taxista, id_usuario FROM corrida" +
                            " GROUP BY id_taxista, id_usuario" +
                                " ORDER BY quantidade DESC LIMIT 10";

    public EstatisticaDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Estatistica obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Estatistica read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Estatistica obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Estatistica> all() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int quantidadeCorrida(){
        try {
            PreparedStatement statement = connection.prepareStatement(corridaQuantidadeQuerry);
            ResultSet result =statement.executeQuery();
            int quantidade;
            if(result.next()){
            quantidade = result.getInt("quantidade");
            return quantidade;
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public List<Estatistica> distanciaMin(){
        try {
            PreparedStatement statement = connection.prepareStatement(distanciaMinQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> distanciasMin = new ArrayList<Estatistica>();
            Estatistica distanciaMin;
            while(result.next()){
                distanciaMin = new Estatistica();
                distanciaMin.setDistanciaCorrida(result.getDouble("distancia_corrida"));
                distanciaMin.setIdTaxista(result.getInt("id_taxista"));
                distanciaMin.setIdUsuario(result.getInt("id_usuario"));
                distanciaMin.setNotaTaxista(result.getInt("nota_taxista"));
                distanciaMin.setNotaUsuario(result.getInt("nota_usuario"));
                distanciaMin.setValorKm(result.getFloat("valorkm"));
                distanciaMin.setPrecoCorrida(result.getFloat("preco_corrida"));
                distanciasMin.add(distanciaMin);
            }
            return distanciasMin;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Estatistica> distanciaMax(){
        try {
            PreparedStatement statement = connection.prepareStatement(distanciaMaxQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> distanciasMax = new ArrayList<Estatistica>();
            Estatistica distanciaMax;
            while(result.next()){
                distanciaMax = new Estatistica();
                distanciaMax.setDistanciaCorrida(result.getDouble("distancia_corrida"));
                distanciaMax.setIdTaxista(result.getInt("id_taxista"));
                distanciaMax.setIdUsuario(result.getInt("id_usuario"));
                distanciaMax.setNotaTaxista(result.getInt("nota_taxista"));
                distanciaMax.setNotaUsuario(result.getInt("nota_usuario"));
                distanciaMax.setValorKm(result.getFloat("valorkm"));
                distanciaMax.setPrecoCorrida(result.getFloat("preco_corrida"));
                distanciasMax.add(distanciaMax);
            }
            return distanciasMax;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public float distanciaAVG(){
        try {
            PreparedStatement statement = connection.prepareStatement(distanciaAVGQuerry);
            ResultSet result =  statement.executeQuery();
            if(result.next()){
            float media = result.getFloat("media");
            return media;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public List<Estatistica> kmMin(){
        try {
            PreparedStatement statement = connection.prepareStatement(kmMinQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> kmsMin = new ArrayList<Estatistica>();
            Estatistica kmMin;
            while(result.next()){
                kmMin = new Estatistica();
                kmMin.setDistanciaCorrida(result.getDouble("distancia_corrida"));
                kmMin.setIdTaxista(result.getInt("id_taxista"));
                kmMin.setIdUsuario(result.getInt("id_usuario"));
                kmMin.setNotaTaxista(result.getInt("nota_taxista"));
                kmMin.setNotaUsuario(result.getInt("nota_usuario"));
                kmMin.setValorKm(result.getFloat("valorkm"));
                kmMin.setPrecoCorrida(result.getFloat("preco_corrida"));
                kmsMin.add(kmMin);
            }
            return kmsMin;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Estatistica> kmMax(){
        try {
            PreparedStatement statement = connection.prepareStatement(kmMaxQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> kmsMax = new ArrayList<Estatistica>();
            Estatistica kmMax;
            while(result.next()){
                kmMax = new Estatistica();
                kmMax.setDistanciaCorrida(result.getDouble("distancia_corrida"));
                kmMax.setIdTaxista(result.getInt("id_taxista"));
                kmMax.setIdUsuario(result.getInt("id_usuario"));
                kmMax.setNotaTaxista(result.getInt("nota_taxista"));
                kmMax.setNotaUsuario(result.getInt("nota_usuario"));
                kmMax.setValorKm(result.getFloat("valorkm"));
                kmMax.setPrecoCorrida(result.getFloat("preco_corrida"));
                
                kmsMax.add(kmMax);
            }
            return kmsMax;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public float kmAVG(){
        try {
            PreparedStatement statement = connection.prepareStatement(kmAVGQuerry);
            ResultSet result =  statement.executeQuery();
            if(result.next()){
            float media = result.getFloat("media");
            return media;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public List<Estatistica> precoMax(){
        try {
            PreparedStatement statement = connection.prepareStatement(precoMaxQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> precosMax = new ArrayList<Estatistica>();
            Estatistica precoMax;
            while(result.next()){
                precoMax = new Estatistica();
                precoMax.setDistanciaCorrida(result.getDouble("distancia_corrida"));
                precoMax.setIdTaxista(result.getInt("id_taxista"));
                precoMax.setIdUsuario(result.getInt("id_usuario"));
                precoMax.setNotaTaxista(result.getInt("nota_taxista"));
                precoMax.setNotaUsuario(result.getInt("nota_usuario"));
                precoMax.setValorKm(result.getFloat("valorkm"));
                precoMax.setPrecoCorrida(result.getFloat("preco_corrida"));
                
                precosMax.add(precoMax);
            }
            return precosMax;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Estatistica> precoMin(){
        try {
            PreparedStatement statement = connection.prepareStatement(precoMinQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> precosMin = new ArrayList<Estatistica>();
            Estatistica precoMin;
            while(result.next()){
                precoMin = new Estatistica();
                precoMin.setDistanciaCorrida(result.getDouble("distancia_corrida"));
                precoMin.setIdTaxista(result.getInt("id_taxista"));
                precoMin.setIdUsuario(result.getInt("id_usuario"));
                precoMin.setNotaTaxista(result.getInt("nota_taxista"));
                precoMin.setNotaUsuario(result.getInt("nota_usuario"));
                precoMin.setValorKm(result.getFloat("valorkm"));
                precoMin.setPrecoCorrida(result.getFloat("preco_corrida"));
                
                precosMin.add(precoMin);
            }
            return precosMin;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public float precoAVG(){
        try {
            PreparedStatement statement = connection.prepareStatement(precoAVGQuerry);
            ResultSet result =  statement.executeQuery();
            if(result.next()){
                float media = result.getFloat("media");
                return media;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    public List<Estatistica> marcaVeiculo(String marca){
        try {
            PreparedStatement statement = connection.prepareStatement(marcaVeiculoQuerry);
            statement.setString(1, marca);
            ResultSet result = statement.executeQuery();
            
            List<Estatistica> marcaVeiculos = new ArrayList<Estatistica>();
            Estatistica marcaVeiculo;
            while(result.next()){
                marcaVeiculo = new Estatistica();
                marcaVeiculo.setDistanciaCorrida(result.getDouble("distancia_corrida"));
                marcaVeiculo.setIdTaxista(result.getInt("id_taxista"));
                marcaVeiculo.setIdUsuario(result.getInt("id_usuario"));
                marcaVeiculo.setNotaTaxista(result.getInt("nota_taxista"));
                marcaVeiculo.setNotaUsuario(result.getInt("nota_usuario"));
                marcaVeiculo.setValorKm(result.getFloat("valorkm"));
                marcaVeiculo.setPrecoCorrida(result.getFloat("preco_corrida"));
                marcaVeiculo.setMarca(result.getString("marca"));
                marcaVeiculo.setModelo(result.getString("modelo"));
                marcaVeiculo.setPlaca(result.getString("placa"));
                
                marcaVeiculos.add(marcaVeiculo);
            }
            return marcaVeiculos;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
     public List<Estatistica> modeloVeiculo(String modelo){
        try {
            PreparedStatement statement = connection.prepareStatement(modeloVeiculoQuerry);
            statement.setString(1, modelo);
            ResultSet result = statement.executeQuery();
            
            List<Estatistica> modeloVeiculos = new ArrayList<Estatistica>();
            Estatistica modeloVeiculo;
            while(result.next()){
                modeloVeiculo = new Estatistica();
                modeloVeiculo.setDistanciaCorrida(result.getDouble("distancia_corrida"));
                modeloVeiculo.setIdTaxista(result.getInt("id_taxista"));
                modeloVeiculo.setIdUsuario(result.getInt("id_usuario"));
                modeloVeiculo.setNotaTaxista(result.getInt("nota_taxista"));
                modeloVeiculo.setNotaUsuario(result.getInt("nota_usuario"));
                modeloVeiculo.setValorKm(result.getFloat("valorkm"));
                modeloVeiculo.setPrecoCorrida(result.getFloat("preco_corrida"));
                modeloVeiculo.setMarca(result.getString("marca"));
                modeloVeiculo.setModelo(result.getString("modelo"));
                modeloVeiculo.setPlaca(result.getString("placa"));
                
                modeloVeiculos.add(modeloVeiculo);
            }
            return modeloVeiculos;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
       
    }
     
    public List<Estatistica> top10TaxistaCorrida(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10TaxistaCorridaQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> top10List = new ArrayList<Estatistica>();
            Estatistica top10;
            while(result.next()){
                top10 = new Estatistica();
                top10.setTaxistaNome(result.getString("nome"));
                top10List.add(top10);
            }
            return top10List;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Estatistica> top10UsuarioPg(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10UsuarioPgQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> top10List = new ArrayList<Estatistica>();
            Estatistica top10;
            while(result.next()){
                top10 = new Estatistica();
                top10.setUsuarioNome(result.getString("nome"));
                System.out.println(top10.getUsuarioNome());
                top10List.add(top10);
            }
            return top10List;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Estatistica> top10TaxistaRb(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10TaxistaRbQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> top10List = new ArrayList<Estatistica>();
            Estatistica top10;
            while(result.next()){
                top10 = new Estatistica();
                top10.setTaxistaNome(result.getString("nome"));
                top10List.add(top10);
            }
            return top10List;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Estatistica> top10TaxistaNota(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10TaxistaNotaQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> top10List = new ArrayList<Estatistica>();
            Estatistica top10;
            while(result.next()){
                top10 = new Estatistica();
                top10.setTaxistaNome(result.getString("nome"));
                top10.setMediaNota(result.getFloat("media"));
                top10List.add(top10);
            }
            return top10List;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Estatistica> top10UsuarioNota(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10UsuarioNotaQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> top10List = new ArrayList<Estatistica>();
            Estatistica top10;
            while(result.next()){
                top10 = new Estatistica();
                top10.setUsuarioNome(result.getString("nome"));
                top10.setMediaNota(result.getFloat("media"));
                top10List.add(top10);
            }
            return top10List;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Estatistica> top10Cliente(){
        try {
            PreparedStatement statement = connection.prepareStatement(top10ClienteQuerry);
            ResultSet result = statement.executeQuery();
            List<Estatistica> top10List = new ArrayList<Estatistica>();
            Estatistica top10;
            while(result.next()){
                top10 = new Estatistica();
                top10.setIdTaxista(result.getInt("id_taxista"));
                top10.setIdUsuario(result.getByte("id_usuario"));
                top10.setQuantidadeCorrida(result.getInt("quantidade"));
                top10List.add(top10);
            }
            return top10List;
            
        } catch (SQLException ex) {
            Logger.getLogger(EstatisticaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
