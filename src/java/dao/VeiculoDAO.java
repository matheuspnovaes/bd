/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Veiculo;

/**
 *
 * @author pedro
 */
public class VeiculoDAO extends DAO<Veiculo> {
    
    private final String createQuery = "INSERT INTO veiculo(taxista_id, placa, renavam, marca, modelo, ano, cor, foto, capacidade_passageiro) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String allQuery = "SELECT id, placa FROM veiculo;";
                                           
    
    private final String readQuery = "SELECT * FROM veiculo WHERE id = ?;";
    
    private final String updateQuery = "UPDATE veiculo "
                                     + "SET placa = ?, renavam = ?, marca = ?, modelo = ?, ano = ?, cor = ?, capacidade_passageiro = ? "
                                     + "WHERE id = ?;";
    
    private final String deleteQuery  = "DELETE FROM veiculo WHERE id = ?;";
    
    private final String imgQuery = "UPDATE veiculo SET foto = ? WHERE id = ?;";
    
    private final String dadosVeiculoQuery = "SELECT * FROM veiculo WHERE taxista_id = ?;";
    
    

    public VeiculoDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Veiculo veiculo) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setInt(1, veiculo.getTaxistaID());
            statement.setString(2, veiculo.getPlaca());
            statement.setString(3, veiculo.getRenavam());
            statement.setString(4, veiculo.getMarca());
            statement.setString(5, veiculo.getModelo());
            statement.setString(6, veiculo.getAno());
            statement.setString(7, veiculo.getCor());
            statement.setString(8, veiculo.getFoto());
            statement.setInt(9, veiculo.getCapacidade());
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                veiculo.setId(result.getInt("id"));
            }
            
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Veiculo read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Veiculo veiculo = new Veiculo();
            
            if (result.next()) {
                veiculo.setId(result.getInt("id"));
                veiculo.setTaxistaID(result.getInt("taxista_id"));
                veiculo.setPlaca(result.getString("placa"));
                veiculo.setRenavam(result.getString("renavam"));
                veiculo.setMarca(result.getString("marca"));
                veiculo.setModelo(result.getString("modelo"));
                veiculo.setAno(result.getString("ano"));
                veiculo.setCor(result.getString("cor"));
                veiculo.setFoto(result.getString("foto"));
                veiculo.setCapacidade(result.getInt("capacidade_passageiro"));
            }
            
            return veiculo;
            
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return null;
        
    }

    @Override
    public void update(Veiculo veiculo) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(1, veiculo.getPlaca());
            statement.setString(2, veiculo.getRenavam());
            statement.setString(3, veiculo.getMarca());
            statement.setString(4, veiculo.getModelo());
            statement.setString(5, veiculo.getAno());
            statement.setString(6, veiculo.getCor());
            statement.setInt(7, veiculo.getCapacidade());
            statement.setInt(8, veiculo.getId());
            
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Veiculo> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Veiculo> veiculos = new ArrayList<Veiculo>();
            
            Veiculo veiculo;
            
            while (result.next()) {
                veiculo = new Veiculo();
                
                veiculo.setId(result.getInt("id"));
                veiculo.setPlaca(result.getString("placa"));
                
                veiculos.add(veiculo);
            }
            
            return veiculos;
            
        } catch (SQLException ex) {
            Logger.getLogger(Veiculo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public void uploadFoto(Veiculo veiculo){
        try {
            PreparedStatement statement = connection.prepareStatement(imgQuery);
            
            statement.setString(1, veiculo.getFoto());
            statement.setInt(2, veiculo.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Veiculo dadosVeiculo(int id_taxista){
        try {
            PreparedStatement statement = connection.prepareStatement(dadosVeiculoQuery);
            
            statement.setInt(1, id_taxista);
            
            ResultSet result = statement.executeQuery();
            
            Veiculo veiculo = new Veiculo();
            
            if (result.next()) {
                veiculo.setId(result.getInt("id"));
                veiculo.setTaxistaID(result.getInt("taxista_id"));
                veiculo.setPlaca(result.getString("placa"));
                veiculo.setRenavam(result.getString("renavam"));
                veiculo.setMarca(result.getString("marca"));
                veiculo.setModelo(result.getString("modelo"));
                veiculo.setAno(result.getString("ano"));
                veiculo.setCor(result.getString("cor"));
                veiculo.setFoto(result.getString("foto"));
                veiculo.setCapacidade(result.getInt("capacidade_passageiro"));
            }
            
            return veiculo;
            
        } catch (SQLException ex) {
            Logger.getLogger(TaxistaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return null;
        
    }

    
    
}
