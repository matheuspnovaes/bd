/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Corrida;
import model.CorridaRequisicao;
import model.Rota;
import model.Usuario;

/**
 *
 * @author pedro
 */
public class UsuarioDAO extends DAO<Usuario> {
    
    private final String createQuery = "INSERT INTO usuario(nome, login, senha, nascimento, cpf, telefone, sexo,foto) "
            + "VALUES (?, ?, md5(?), ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String allQuery = "SELECT id, nome FROM usuario;";
    
    private final String readQuery = "SELECT * FROM usuario WHERE id = ?;";
    
    private final String updateQuery = "UPDATE usuario "
                                     + "SET nome = ?, login = ?, senha = md5(?), nascimento = ? "
                                     + "WHERE id = ?;";
    
    private final String deleteQuery  = "DELETE FROM usuario WHERE id = ?;";
    
    private final String authenticateQuery = "SELECT * "
                                           + "FROM usuario "
                                            + "WHERE login = ? AND senha = md5(?);";
    private final String imgQuery = "UPDATE usuario SET foto = ? WHERE id = ?;";
    private final String findTaxiQuery = "SELECT DISTINCT id_rotas, id_taxista,latitude,longitude FROM (SELECT *,(6371 * acos( cos ( radians(?)) * cos(radians(latitude)) *cos(radians(?) - radians(longitude)) + sin(radians(?)) * sin(radians(latitude)))) AS distance FROM pontos ) AS distances, taxista t, rotas r WHERE distance < 9 and t.id = r.id_taxista AND r.id = distances.id_rotas;";
    
    private final String reqTaxi = "INSERT INTO requisicao_corrida(id_usuario, id_taxista, latitude, longitude, status, lat, lng) VALUES (?, ?, ?, ?, ?, ?, ?) RETURNING id;";
    
    private final String statusCorrida = "SELECT * FROM requisicao_corrida WHERE id  = ?;";
    //private final String inserir = "COPY coordenadas FROM '/tmp/?.txt' using delimiters ','";
    private final String dadosCorridaQuery = "SELECT * FROM corrida WHERE id_requisicao = ?;";
    private final String validarCorridaQuery = "UPDATE corrida SET nota_taxista = ? WHERE id_requisicao = ?;";
    private final String historicoCorridaQuery = "SELECT * FROM corrida WHERE id_usuario = ?;";
    private final String rotaQuery = "SELECT * FROM pontos WHERE id_rotas = ?;";
    private final String rotaDestino = "SELECT id_rota FROM requisicao_corrida WHERE id = ?;";
    
    public UsuarioDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Usuario usuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(createQuery);
            
            statement.setString(1, usuario.getNome());
            statement.setString(2, usuario.getLogin());
            statement.setString(3, usuario.getSenha());
            statement.setDate(4, usuario.getDataNascimento());
            statement.setString(5, usuario.getCpf());
            statement.setString(6, usuario.getTelefone());
            statement.setString(7, usuario.getSexo());
            statement.setString(8, usuario.getFoto());
            
            ResultSet result = statement.executeQuery();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Usuario read(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(readQuery);
            
            statement.setInt(1, id);
            
            ResultSet result = statement.executeQuery();
            
            Usuario usuario = new Usuario();
            
            if (result.next()) {
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setDataNascimento(result.getDate("nascimento"));
                usuario.setFoto(result.getString("foto"));
            }
            
            return usuario;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return null;
        
    }

    @Override
    public void update(Usuario usuario) {
        try {
            PreparedStatement statement = connection.prepareStatement(updateQuery);
            
            statement.setString(1, usuario.getNome());
            statement.setString(2, usuario.getLogin());
            statement.setString(3, usuario.getSenha());
            statement.setDate(4, usuario.getDataNascimento());
            
            statement.setInt(5, usuario.getId());
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement statement = connection.prepareStatement(deleteQuery);
            
            statement.setInt(1, id);
            
            statement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Usuario> all() {
        try {
            PreparedStatement statement = connection.prepareStatement(allQuery);
            
            ResultSet result = statement.executeQuery();
            
            List<Usuario> usuarios = new ArrayList<Usuario>();
            
            Usuario usuario;
            
            while (result.next()) {
                usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                
                usuarios.add(usuario);
            }
            
            return usuarios;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public Usuario authenticate(String login, String password) {
         try {
            
            PreparedStatement statement = connection.prepareStatement(authenticateQuery);
            
            statement.setString(1, login);
            statement.setString(2, password);
            
            ResultSet result  = statement.executeQuery();
            
            if (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                usuario.setLogin(result.getString("login"));
                usuario.setSenha(result.getString("senha"));
                usuario.setDataNascimento(result.getDate("nascimento"));
                usuario.setTipo(result.getInt("tipo"));
                return usuario;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return null;
    }

    public void uploadFoto(Usuario usuario){
        try {
            PreparedStatement statement = connection.prepareStatement(imgQuery);
            
            statement.setString(1, usuario.getFoto());
            statement.setInt(2, usuario.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public List<Rota> buscarTaxi(double latitude, double longitude){
        try {
            PreparedStatement statement = connection.prepareStatement(findTaxiQuery);
            statement.setDouble(1, latitude);
            statement.setDouble(2, longitude);
            statement.setDouble(3, latitude);
            ResultSet result = statement.executeQuery();
             List<Rota> rotas = new ArrayList<Rota>();
             Rota rota;
            while(result.next()){
                 rota = new Rota();
                 rota.setIdRota(result.getInt("id_rotas"));
                 rota.setLatitude(result.getDouble("latitude"));
                 rota.setLongitude(result.getDouble("longitude"));
                 //rota.setObserv(result.getTimestamp("observ"));
                 rota.setIdTaxista(result.getInt("id_taxista"));
                 rotas.add(rota);
            }
            
            return rotas;    
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int requisicaoTaxi(int id_taxi, int id_usuario, double latitude, double longitude, double lat, double lng){
        try {
            PreparedStatement statement  = connection.prepareStatement(reqTaxi);
            int status = 0;
            statement.setInt(1, id_usuario);
            statement.setInt(2, id_taxi);
            statement.setDouble(3, latitude);
            statement.setDouble(4, longitude);
            statement.setInt(5, status);
            statement.setDouble(6, lat);
            statement.setDouble(7, lng);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                int id = result.getInt("id");
                return id;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    public List<CorridaRequisicao> statusCorrida(int id_requisicao){
        try {
            PreparedStatement statement = connection.prepareStatement(statusCorrida);
            statement.setInt(1, id_requisicao);
            ResultSet result = statement.executeQuery();
            
            List<CorridaRequisicao> corridasRq = new ArrayList<CorridaRequisicao>();
             CorridaRequisicao corridaRq ;
            while(result.next()){
                corridaRq = new CorridaRequisicao();
                corridaRq.setId_requisicao(result.getInt("id"));
                corridaRq.setId_taxista(result.getInt("id_taxista"));
                corridaRq.setId_usuario(result.getInt("id_usuario"));
                corridaRq.setLatitude(result.getDouble("latitude"));
                corridaRq.setLongitude(result.getDouble("longitude"));
                corridaRq.setStatus(result.getInt("status"));
                corridasRq.add(corridaRq);
            }
            return corridasRq;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
       return null;
    }
    public Corrida dadosCorrida(int id_requisição){
        try {
            PreparedStatement statement = connection.prepareStatement(dadosCorridaQuery);
            
            statement.setInt(1, id_requisição);
            
            ResultSet result = statement.executeQuery();
            Corrida corrida = new Corrida();
            while(result.next()){
                corrida.setId(result.getInt("id"));
                corrida.setDistancia_corrida(result.getDouble("distancia_corrida"));
                corrida.setId_requisicao(result.getInt("id_requisicao"));
                corrida.setId_taxista(result.getInt("id_taxista"));
                corrida.setId_usuario(result.getInt("id_usuario"));
                corrida.setValor_corrida(result.getFloat("preco_corrida"));
                corrida.setValorkm(result.getFloat("valorkm"));  
            }
            return corrida;
            } catch (SQLException ex) {
                 Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void validarCorrida(int id_requisicao, int nota_taxista){
        try {
            PreparedStatement statement = connection.prepareStatement(validarCorridaQuery);
            statement.setInt(1, nota_taxista);
            statement.setInt(2, id_requisicao);
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public   Rota rotaDestino(int id_requisicao){
        try {
            PreparedStatement statement = connection.prepareStatement(rotaDestino);
            statement.setInt(1, id_requisicao);
          
            ResultSet result = statement.executeQuery();
            Rota rota = new Rota();
            if(result.next()){
                rota.setIdRota(result.getInt("id_rota"));
                return rota;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<Rota> rota(int id_rota){
        try {
            PreparedStatement statement = connection.prepareStatement(rotaQuery);
            statement.setInt(1, id_rota);
            
            
            ResultSet result = statement.executeQuery();
            
            List<Rota> rotas = new ArrayList<Rota>();
            Rota rota;
            
            while(result.next()){
                rota = new Rota();
                
                rota.setIdRota(result.getInt("id_rotas"));
                rota.setLatitude(result.getDouble("latitude"));
                rota.setLongitude(result.getDouble("longitude"));
                rota.setObserv(result.getTimestamp("observ"));
                rotas.add(rota);
            }
            return rotas;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<Corrida> historicoCorrida(int id_usuario){
        
        try {
            PreparedStatement statement = connection.prepareStatement(historicoCorridaQuery);
            
            statement.setInt(1, id_usuario);
            ResultSet result = statement.executeQuery();
            
            List<Corrida> corridas = new ArrayList<Corrida>();
            Corrida corrida;
            
            while(result.next()){
                corrida = new Corrida(); 
                System.out.println(result.getInt("id_usuario"));
                corrida.setId(result.getInt("id"));
                corrida.setDistancia_corrida(result.getDouble("distancia_corrida"));
                corrida.setNota_taxista(result.getInt("nota_taxista"));
                corrida.setNota_usuario(result.getInt("nota_taxista"));
                corrida.setId_requisicao(result.getInt("id_requisicao"));
                corrida.setId_taxista(result.getInt("id_taxista"));
                corrida.setId_usuario(result.getInt("id_usuario"));
                corrida.setValor_corrida(result.getFloat("preco_corrida"));
                corrida.setValorkm(result.getFloat("valorkm"));
                
                corridas.add(corrida);
            }
            
            return corridas;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
