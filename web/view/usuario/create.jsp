<%-- 
    Document   : create
    Created on : 22/10/2015, 17:30:01
    Author     : pedro
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%--<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>


<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        
        <%@include file="/view/includes/menu.jsp" %>
        
        <h1>Cadastro de usuário</h1>
        
        <form method="post" class="form-horizontal"  action="${pageContext.servletContext.contextPath}/usuario/create" >
            <fieldset>

                <!-- Form Name -->
                <legend>Cadastro</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nome">Nome</label>  
                    <div class="col-md-4">
                        <input id="nome" name="nome" type="text" placeholder="Digite seu nome" class="form-control input-md" required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nascimento">Data de Nascimento</label>  
                    <div class="col-md-4">
                        <input id="nascimento" name="nascimento" type="text" placeholder="Digite sua data de nascimento" class="form-control input-md" required="">
                        <span class="help-block">Ex: 04/01/1995</span>  
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="telefone">Telefone</label>  
                    <div class="col-md-4">
                        <input id="telefone" name="telefone" type="text" placeholder="Digite seu telefone" class="form-control input-md" required="">

                    </div>
                </div>
                
                 <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="cpf">CPF</label>  
                    <div class="col-md-4">
                        <input id="cpf" name="cpf" type="text" placeholder="Digite seu cpf" class="form-control input-md" required="">

                    </div>
                </div>
                <!-- Multiple Radios -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="sexo">Sexo</label>
                    <div class="col-md-4">
                        <div class="radio">
                            <label for="sexo-0">
                                <input type="radio" name="sexo" id="sexo-0" value="M" checked="checked">
                                Masculino
                            </label>
                        </div>
                        <div class="radio">
                            <label for="sexo-1">
                                <input type="radio" name="sexo" id="sexo-1" value="F">
                                Feminino
                            </label>
                        </div>
                    </div>
                </div>

              

              

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="login">Login</label>  
                    <div class="col-md-4">
                        <input id="login" name="login" type="text" placeholder="Digite um login" class="form-control input-md" required="">

                    </div>
                </div>
                
                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="senha">Senha</label>
                    <div class="col-md-4">
                        <input id="senha" name="senha" type="password" placeholder="Digite uma senha" class="form-control input-md" required="">

                    </div>
                </div>

                <div class="form-group">
                     <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4">
                        <input type="submit" value="Enviar" class="btn btn-primary" />
                    </div>
                </div>
            </fieldset>
        </form>
             
        
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
