<%-- 
    Document   : chamarTaxi
    Created on : 10/12/2015, 19:35:13
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Listagem Usuários</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/leaflet.css"/> 

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Taxista Disponível</h1>
                </div>
               
            </div>

            <div class="row">
                <div class="col-md-10">
                    
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div  class="col-md-10">
                                <div id="my-map">      
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${rotas}" var="u">
                                <tr>
                                    <td>${u.idTaxista}</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/usuario/chamarTaxi?id=${u.idTaxista}&lat=${u.latitude}&lng=${u.longitude}"> Chamar Táxi</a>
                                        
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
         <%@include file="/view/includes/scripts.jsp" %>
        <script src="${pageContext.servletContext.contextPath}/assets/js/leaflet.js"></script> 
        <script src="${pageContext.servletContext.contextPath}/assets/js/rota.js"></script>
        

    </body>
</html>
