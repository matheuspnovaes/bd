<%-- 
    Document   : index
    Created on : 22/10/2015, 17:01:53
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Listagem Usuários</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Listagem dos usuários</h1>
                </div>
                
                <c:choose>
                    <c:when test="${not empty sessionScope.usuario}" >
                <div class="col-md-2 col-xs-2">
                    <a href="${pageContext.servletContext.contextPath}/taxista/create" class="right btn btn-primary">Cadastrar Táxi</a>
                </div>
                <div class="col-md-2 col-xs-2">
                    <a href="${pageContext.servletContext.contextPath}/usuario/foto" class="right btn btn-primary">Adicionar/Editar foto</a>
                    </c:when>
                    <c:otherwise>
                        
                    </c:otherwise>
                </c:choose>
                </div>
            </div>

            <div class="row">
                <div class="col-md-10">
                    <div class="col-md-4">
                                    <a href="#" class="thumbnail">
                                        <img class="usuario-img"
                                             src="${pageContext.servletContext.contextPath}/assets/img/${usuario.foto}"/>
                                    </a>
                                    <p> Nome: ${usuario.nome}</p>
                                    <p> login: ${usuario.login}</p>
                                    <p></p>
                </div> 
            </div>
        </div>
         <%@include file="/view/includes/scripts.jsp" %>       
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/main.js" ></script>
        

    </body>
</html>
