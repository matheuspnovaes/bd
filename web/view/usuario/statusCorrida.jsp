<%--
    Document   : statusCorrida
    Created on : 11/12/2015, 10:34:11
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Status Corrida</title>
        <%@include file="/view/includes/head.jsp" %>
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/leaflet.css"/>
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>
         

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Taxista Disponível</h1>
                </div>
            </div>


            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nome Taxista</th>
                                <th>Status Corrida</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${corridasRq}" var="c">

                                <tr>

                                    <td>${c.id_taxista}</td>
                                   
                                    <c:choose>
                                        <c:when test="${c.status == 0}" >
                                            <td>
                                                Aguardando reposta
                                            </td>
                                        </c:when>
                                        <c:when test="${c.status == 1}" >
                                            <td>
                                                Corrida Aceita 
                                            </td>
                                        </c:when>
                                        <c:when test="${c.status == 2}" >
                                            <td>
                                                Corrida Rejeitada
                                            </td>
                                        </c:when>
                                        <c:when test="${c.status == 3}" >
                                            <td>
                                                <form method="post" class="form-horizontal"  action="${pageContext.servletContext.contextPath}/usuario/finalizar" >
            <fieldset>

                

                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="latitude">Latitude</label>  
                    <div class="col-md-4">
                        <input id="latitude" name="latitude" placeholder="Digite sua latitude" class="form-control input-md" required="" type="text">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="longitude">Longitude</label>  
                    <div class="col-md-4">
                        <input id="longitude" name="longitude" placeholder="Digite sua longitude" class="form-control input-md" required="" type="text">

                    </div>
                </div>
                

                <div class="form-group">
                     <label class="col-md-4 control-label" for="enviar"></label>
                    <div class="col-md-4">
                        <input type="submit" value="Enviar" class="btn btn-primary" />
                    </div>
                </div>
            </fieldset>
        </form> 
                                            </td>
                                        </c:when>
                                    </c:choose>

                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div> 
            <div class="row">
                    <div class="col-md-10">
                        <div id="my-map">      
                        </div>
                    </div>
                </div>
        </div>
         <%@include file="/view/includes/scripts.jsp" %>
        <script src="${pageContext.servletContext.contextPath}/assets/js/leaflet.js"></script> 
        <script src="${pageContext.servletContext.contextPath}/assets/js/destino.js"></script>

    </body>
</html>
