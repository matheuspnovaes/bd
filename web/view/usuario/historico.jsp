<%-- 
    Document   : historico
    Created on : 20/01/2016, 20:31:24
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Chamadas para corrida </title>
        <%@include file="/view/includes/head.jsp" %>
        
        
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Chamadas Corrida</h1>
                </div>
               
            </div>

            <div class="row">
                <div class="col-md-10">
                    <table class="js-dynamitable  table table-bordered">
                        <thead>
                            <tr>
                                <th>Corrida <span class="js-sorter-desc  glyphicon glyphicon-chevron-down pull-right"></span> <span class="js-sorter-asc glyphicon glyphicon-chevron-up pull-right"></span> </th>
                                <th>Taxista</th>
                                <th>Nota Taxista<span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span> <span class="js-sorter-asc glyphicon glyphicon-chevron-up pull-right"></span> 
                                </th>
                                <th>Nota Usuario<span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span> <span class="js-sorter-asc glyphicon glyphicon-chevron-up pull-right"></span> 
                                </th>
                                <th>Valor por KM<span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span> <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span> 
                                </th>
                                <th>Distância<span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span> <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span> 
                                </th>
                                <th>Preço Corrida<span class="js-sorter-desc     glyphicon glyphicon-chevron-down pull-right"></span> <span class="js-sorter-asc     glyphicon glyphicon-chevron-up pull-right"></span> 
                                </th>
                            </tr>
                                
                            <tr>
                                <th> <!-- input filter -->
                                     <input  class="js-filter  form-control" type="text" value="">
                                </th>
                                 <th> <!-- input filter -->
                                     <input  class="js-filter  form-control" type="text" value="">
                                 </th>
                                <th> <!-- input filter -->
                                     <input  class="js-filter  form-control" type="text" value="">
                                 </th>
                                <th> <!-- input filter -->
                                     <input  class="js-filter  form-control" type="text" value="">
                                </th>
                                <th> <!-- input filter -->
                                     <input  class="js-filter  form-control" type="text" value="">
                                </th>
                                <th> <!-- input filter -->
                                     <input  class="js-filter  form-control" type="text" value="">
                                 </th>
                                 <th> <!-- input filter -->
                                     <input  class="js-filter  form-control" type="text" value="">
                                 </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${corridas}" var="c">
                                <tr>
                                    <td>${c.id}</td>
                                    <td>${c.id_taxista}</td>
                                    <td>${c.nota_taxista}</td>
                                    <td>${c.nota_usuario}</td>
                                    <td>${c.valorkm}</td>
                                    <td>${c.distancia_corrida}</td>
                                    <td>${c.valor_corrida}</td>
                                </tr>
                                
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
         <%@include file="/view/includes/scripts.jsp" %>
         <script src="${pageContext.servletContext.contextPath}/assets/js/dynamitable.jquery.min.js"></script>
         
      
        

    </body>
</html>
