<%-- 
    Document   : foto
    Created on : 22/11/2015, 13:38:28
    Author     : matheus
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%--<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>


<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        
        <%@include file="/view/includes/menu.jsp" %>
        
        <h1>Cadastro de usuário</h1>
        
        <form method="post" class="form-horizontal"  action="${pageContext.servletContext.contextPath}/usuario/foto"  enctype="multipart/form-data">
            <fieldset>

                

                
                <!-- File Button --> 
                <div class="form-group">
                    <label class="col-md-4 control-label" for="foto">Foto</label>
                    <div class="col-md-4">
                        <input id="foto" name="foto" class="input-file" type="file">
                    </div>
                </div>

                

                <div class="form-group">
                     <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4">
                        <input type="submit" value="Enviar" class="btn btn-primary" />
                    </div>
                </div>
            </fieldset>
        </form>
             
        
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
