<%-- 
    Document   : chamarTaxi
    Created on : 10/12/2015, 18:10:14
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%--<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>


<!DOCTYPE html>
<html>
    <head>
        <title>Buscar Táxi</title>
        <%@include file="/view/includes/head.jsp" %>
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/leaflet.css"/> 
        <link  href="${pageContext.servletContext.contextPath}/assets/css/bootstrap-responsive.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        
        <%@include file="/view/includes/menu.jsp" %>
        
        <h1>Buscartaxi</h1>
        <div class="container-fluid">
                <div class="row-fluid">
                    <div  class="col-md-10">
                        <div id="my-map">      
                        </div>
                    </div>
                </div>
        </div> <!-- container -->
        <form method="post" class="form-horizontal"  action="${pageContext.servletContext.contextPath}/usuario/buscarTaxi" >
            <fieldset>

                

                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="latitude">Latitude</label>  
                    <div class="col-md-4">
                        <input id="latitude" name="latitude" placeholder="Digite sua latitude" class="form-control input-md" required="" type="text">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="longitude">Longitude</label>  
                    <div class="col-md-4">
                        <input id="longitude" name="longitude" placeholder="Digite sua longitude" class="form-control input-md" required="" type="text">

                    </div>
                </div>
                

                <div class="form-group">
                     <label class="col-md-4 control-label" for="enviar"></label>
                    <div class="col-md-4">
                        <input type="submit" value="Enviar" class="btn btn-primary" />
                    </div>
                </div>
            </fieldset>
        </form> 

            
        
        <%@include file="/view/includes/scripts.jsp" %>
        <script src="${pageContext.servletContext.contextPath}/assets/js/leaflet.js"></script> 
        <script src="${pageContext.servletContext.contextPath}/assets/js/mapa.js"></script>
    </body>
</html>
