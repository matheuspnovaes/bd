<%-- 
    Document   : index
    Created on : 20/11/2015, 16:31:16
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Taxista</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Listagem dos taxista</h1>
                </div>
            </div>
            <%--
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Preço km</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${taxistas}" var="t">
                                <tr>
                                    <td>${t.id}</td>
                                    <td>${t.precoKm}</td>
                                    <td>
                                        <a  href="${pageContext.servletContext.contextPath}/taxista/update?id=${t.id}"><span class="glyphicon glyphicon-pencil"></span>Editar</a>
                                        |
                                        <a href="#" class="link-delete" data-id="${t.id}" ><span class="glyphicon glyphicon-trash"></span>Deletar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div> --%>
            
            <div class="row">
                <div class="col-md-10">
                    <div class="col-md-4">
                                    <a href="#" class="thumbnail">
                                        <img class="usuario-img"
                                             src="${pageContext.servletContext.contextPath}/assets/img/${usuario.foto}"/>
                                    </a>
                                    <p> Nome: ${usuario.nome}</p>
                                    <p> login: ${usuario.login}</p>
                                    <p> Preço KM:${taxista.precoKm} </p>
                                    <a href="#" class="thumbnail">
                                        <img class="usuario-img"
                                             src="${pageContext.servletContext.contextPath}/assets/img/${veiculo.foto}"/>
                                    </a>
                                    <p>Marca: ${veiculo.marca}</p>
                                    <p>Modelo: ${veiculo.modelo}</p>
                                    <p>Ano: ${veiculo.ano}</p>
                                    <p>Capacidade: ${veiculo.capacidade}</p>
                                    <p>Cor: ${veiculo.cor}</p>
                                    <p>Placa: ${veiculo.placa}</p>
                                    <p>Renavam: ${veiculo.renavam}</p>
                </div> 
            </div>
        </div>
        </div>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/taxista.js" ></script>
        <%@include file="/view/includes/scripts.jsp" %>
       

    </body>
</html>
