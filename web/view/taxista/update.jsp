<%-- 
    Document   : update
    Created on : 20/11/2015, 17:20:38
    Author     : matheus
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>
        <title>Update</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        
        <%@include file="/view/includes/menu.jsp" %>
        
        <h1>Cadastro de usuário</h1>
        
        <form method="post" action="${pageContext.servletContext.contextPath}/taxista/update" >
            
            <input type="hidden" name="id" value="${taxista.id}" />
            <div class="form-group">
                <label>Valor km:</label>
                <input type="text" name="precokm" class="form-control"  value="${taxista.precoKm}" />
            </div>
            
            <input type="submit" value="Enviar" class="btn btn-primary" />
        </form>
        
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
