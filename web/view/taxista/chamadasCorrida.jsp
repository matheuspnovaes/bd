<%-- 
    Document   : chamadasCorrida
    Created on : 11/12/2015, 14:49:24
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Chamadas para corrida </title>
        <%@include file="/view/includes/head.jsp" %>
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/leaflet.css"/> 
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Chamadas Corrida</h1>
                </div>
               
            </div>

            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>latitude</th>
                                <th>Longitude</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${corridasRq}" var="c">
                                <tr>
                                    <td>${c.id_usuario}</td>
                                    <td>${c.latitude}</td>
                                    <td>${c.longitude}</td>
                                    <c:choose>
                                        <c:when test="${c.status == 0}" >
                                            <td>
                                                <a href="${pageContext.servletContext.contextPath}/taxista/responderCliente?id=${c.id_usuario}&i=1&id_rq=${c.id_requisicao}" class="right btn btn-primary">Aceitar</a> |
                                                <a href="${pageContext.servletContext.contextPath}/taxista/responderCliente?id=${c.id_usuario}&i=2&id_rq=${c.id_requisicao}" class="right btn btn-primary">Rejeitar</a>
                                        
                                            </td>
                                        </c:when>
                                        <c:when test="${c.status == 1}" >
                                            <td>
                                                Corrida Aceita
                                            </td>
                                        </c:when>
                                        <c:when test="${c.status == 2}" >
                                            <td>
                                                Corrida Rejeitada
                                            </td>
                                        </c:when>
                                        <c:when test="${c.status == 3}" >
                                            <td>
                                                 <a href="${pageContext.servletContext.contextPath}/taxista/dadosCorrida">finalizar</a>  
                                            </td>
                                        </c:when>
                                    </c:choose>
                                    
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-10">
                        <div id="my-map">      
                        </div>
                    </div>
                </div>
        </div>
         <%@include file="/view/includes/scripts.jsp" %>
        <script src="${pageContext.servletContext.contextPath}/assets/js/leaflet.js"></script> 
        <script src="${pageContext.servletContext.contextPath}/assets/js/mapa.js"></script>
        

    </body>
</html>
