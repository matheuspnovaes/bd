<%-- 
    Document   : create
    Created on : 20/11/2015, 13:14:28
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%--<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>


<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        
        <%@include file="/view/includes/menu.jsp" %>
        
        <h1>Cadastro de táxista</h1>
        
        <form method="post" class="form-horizontal"  action="${pageContext.servletContext.contextPath}/taxista/create" >
            <fieldset>

                <!-- Form Name -->
                <legend>Cadastro</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="placa">Placa</label>  
                    <div class="col-md-4">
                        <input id="placa" name="placa" type="text" placeholder="Digite a placa" class="form-control input-md" required="">
                        <span class="help-block">Ex: AAA-1234</span> 
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="renavam">Renavam</label>  
                    <div class="col-md-4">
                        <input id="renavam" name="renavam" type="text" placeholder="Digite o renavam" class="form-control input-md" required="">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="marca">Marca</label>  
                    <div class="col-md-4">
                        <input id="marca" name="marca" type="text" placeholder="Digite a marca" class="form-control input-md" required="">

                    </div>
                </div>

                

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="modelo">Modelo</label>  
                    <div class="col-md-4">
                        <input id="modelo" name="modelo" type="text" placeholder="Digite o modelo" class="form-control input-md" required="">
                    </div>
                </div>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="ano">Ano</label>  
                    <div class="col-md-4">
                        <input id="ano" name="ano" type="text" placeholder="Digite o ano" class="form-control input-md" required="">
                    </div>
                </div>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="cor">Cor</label>  
                    <div class="col-md-4">
                        <input id="cor" name="cor" type="text" placeholder="Digite a cor" class="form-control input-md" required="">
                    </div>
                </div>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="capacidade">Capacidade Passageiro</label>  
                    <div class="col-md-4">
                        <input id="capacidade" name="capacidade" type="text" placeholder="Digite a capacidade" class="form-control input-md" required="">
                    </div>
                </div>
                
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="modelo">Preço por Km</label>  
                    <div class="col-md-4">
                        <input id="precokm" name="precokm" type="text" placeholder="Digite o preço por Km" class="form-control input-md" required="">
                    </div>
                </div>
                
               
              

                <div class="form-group">
                     <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4">
                        <input type="submit" value="Enviar" class="btn btn-primary" />
                    </div>
                </div>
            </fieldset>
        </form>
             
        
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
