<%-- 
    Document   : dadosCorrida
    Created on : 13/12/2015, 13:29:36
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Listagem Usuários</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Dados Corridas</h1>
                </div>

                <div class="container"> 
                    
                        <label class="col-md-4 control-label">Usuario : ${usuario.nome}</label>  
                      
                   
                        <label class="col-md-4 control-label">Taixista: ${taxista.nome}</label>  
                   
                    <div>
                        <label class="col-md-4 control-label">Distância : ${corrida.distancia_corrida}</label>  
                    </div>    
                    <div>
                        <label class="col-md-4 control-label">Valor Km rodado : ${corrida.valorkm}</label>  
                    </div>   
                </div>    
                <div>
                    <label class="col-md-4 control-label">Valor corrida : ${corrida.valor_corrida}</label>  
                </div> 

                <form method="post" class="form-horizontal"  action="${pageContext.servletContext.contextPath}/taxista/dadosCorrida">
                    <fieldset>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="usuarionota">Nota</label>
                            <div class="col-md-4">
                                <select id="usuarionota" name="usuarionota" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                             <label class="col-md-4 control-label" for="enviar"></label>
                             <div class="col-md-4">
                                <input type="submit" value="Enviar" class="btn btn-primary" />
                            </div>
                        </div>
                    </fieldset>
                    
                </form>
            </div>
        </div>
        <%@include file="/view/includes/scripts.jsp" %>       
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/main.js" ></script>
 

</body>
</html>

