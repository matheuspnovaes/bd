<%-- 
    Document   : update
    Created on : 20/11/2015, 23:35:06
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if>
--%>
<!DOCTYPE html>
<html>
    <head>
        <title>Update</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        
        <%@include file="/view/includes/menu.jsp" %>
        
        <h1>Cadastro de usuário</h1>
        
        <form method="post" action="${pageContext.servletContext.contextPath}/veiculo/update" >
            
            <input type="hidden" name="id" value="${veiculo.id}" />
            
            <div class="form-group">
                <label>Placa:</label>
                <input type="text" name="placa" class="form-control" value="${veiculo.placa}" />
            </div>
            
            <div class="form-group">
                <label>Renavam: </label>
                <input type="text" name="renavam" class="form-control" value="${veiculo.renavam}" />
            </div>
            
            
            <div class="form-group">
                <label>Marca: </label>
                <input type="text" name="marca" class="form-control" value="${veiculo.marca}" />
            </div>
            
            <div class="form-group">
                <label>Modelo:</label>
                <input type="text" name="modelo" class="form-control"  value="${veiculo.modelo}" />
            </div>
            <div class="form-group">
                <label>Ano:</label>
                <input type="text" name="ano" class="form-control"  value="${veiculo.ano}" />
            </div>
            <div class="form-group">
                <label>Cor:</label>
                <input type="text" name="cor" class="form-control"  value="${veiculo.cor}" />
            </div>
            <div class="form-group">
                <label>Quantidade Passageiro:</label>
                <input type="text" name="capacidade" class="form-control"  value="${veiculo.capacidade}" />
            </div>
            <input type="submit" value="Enviar" class="btn btn-primary" />
        </form>
        
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
