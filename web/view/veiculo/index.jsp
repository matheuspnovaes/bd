<%-- 
    Document   : index.jsp
    Created on : 20/11/2015, 23:25:43
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Listagem Veículos</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Listagem dos veiculos</h1>
                </div>
                
            </div>

            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Placa</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${veiculos}" var="v">
                                <tr>
                                    <td>${v.id}</td>
                                    <td>${v.placa}</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/veiculo/update?id=${v.id}"><span class="glyphicon glyphicon-pencil">Editar</a>
                                        |
                                        <a href="#" class="link-delete" data-id="${v.id}"><span class="glyphicon glyphicon-trash"></span>Deletar</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/veiculo.js" ></script>        
        <%@include file="/view/includes/scripts.jsp" %>
        

    </body>
</html>