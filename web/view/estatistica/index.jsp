<%-- 
    Document   : index
    Created on : 24/01/2016, 22:01:35
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Dados Estatistico</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/leaflet.css"/> 

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Dados Estatistico</h1>
                </div>
               
            </div>

            <div class="row">
                <div class="col-md-10">
                    
                    
                    </div>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Dado</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                                
                                <tr>
                                    <td>Distancia Corrida</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/distanciaCorrida">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Preço por Km</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/kmCorrida">Visualizar</a>  
                                    </td>
                                </tr>   
                                <tr>
                                    <td>Corridas por Marca</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/selectMarca">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Corridas por Modelo</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/selectModelo">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Total de Corridas</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/quantidadeCorrida">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Top 10 Cliente Fidelidade</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/top10Cliente">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Top10 Taxista Corrida</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/top10TaxistaCorrida">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Top10 Taxista Nota</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/top10TaxistaNota">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Top10 Cliente Nota</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/top10UsuarioNota">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Top10 Taxista Recebimento</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/top10TaxistaRb">Visualizar</a>  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Top10 Cliente Pagamento</td>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/estatistica/top10UsuarioPg">Visualizar</a>  
                                    </td>
                                </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
         <%@include file="/view/includes/scripts.jsp" %>
        <script src="${pageContext.servletContext.contextPath}/assets/js/leaflet.js"></script> 
        <script src="${pageContext.servletContext.contextPath}/assets/js/rota.js"></script>
        

    </body>
</html>