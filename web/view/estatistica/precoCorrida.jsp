<%-- 
    Document   : precoCorrida
    Created on : 23/01/2016, 09:35:56
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Estatistica para corrida </title>
        <%@include file="/view/includes/head.jsp" %>
        
        
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Estatistica Corrida</h1>
                </div>
               
            </div>
            <div><h3>Preço Minímo Pago</h3></div>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Taxista</th>
                                <th>Usuario</th>
                                <th>Nota Taxista</th>
                                <th>Nota Usuario</th>
                                <th>Valor KM</th>
                                <th>Distancia</th>
                                <th>Valor pago</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${precosMin}" var="d">
                                <tr>
                                    <td>${d.idTaxista}</td>
                                    <td>${d.idUsuario}</td>
                                    <td>${d.notaTaxista}</td>
                                    <td>${d.notaUsuario}</td>
                                    <td>${d.valorKm}</td>
                                    <td>${d.distanciaCorrida}</td>
                                    <td>${d.precoCorrida}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div><h3>Preço Máximo Pago</h3></div>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Taxista</th>
                                <th>Usuario</th>
                                <th>Nota Taxista</th>
                                <th>Nota Usuario</th>
                                <th>Valor KM</th>
                                <th>Distancia</th>
                                <th>Valor pago</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${precosMax}" var="d">
                                <tr>
                                    <td>${d.idTaxista}</td>
                                    <td>${d.idUsuario}</td>
                                    <td>${d.notaTaxista}</td>
                                    <td>${d.notaUsuario}</td>
                                    <td>${d.valorKm}</td>
                                    <td>${d.distanciaCorrida}</td>
                                    <td>${d.precoCorrida}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <div><h3>Preço Médio Pago</h3><p>${mediapreco}</p></div>
        </div>
         <%@include file="/view/includes/scripts.jsp" %>       
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/main.js" ></script>
         
      
        

    </body>
</html>