<%-- 
    Document   : quantidadeCorrida
    Created on : 23/01/2016, 08:39:21
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Estatistica Corrida</title>
        <%@include file="/view/includes/head.jsp" %>
        
        
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Estatistica</h1>
                </div>
               <div class="col-md-10 col-xs-10">
                    <h2>Quantidade de corridas</h2>
                    <p>${quantidade}</p>
                </div>
            </div>

        </div>  
         <%@include file="/view/includes/scripts.jsp" %>
        
         
      
        

    </body>
</html>