<%-- 
    Document   : selectMarca
    Created on : 23/01/2016, 09:58:16
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%--<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>


<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <%@include file="/view/includes/head.jsp" %>
    </head>
    <body>
        
        <%@include file="/view/includes/menu.jsp" %>
        
        <h1>Selecionar Marca</h1>
        
        <form method="post" class="form-horizontal"  action="${pageContext.servletContext.contextPath}/estatistica/marcaVeiculo">
            <fieldset>

                

                <!-- Form Name -->
                <legend>Selecionar Marca</legend>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="marca">Marca</label>
                    <div class="col-md-4">
                        <select id="marca" name="marca" class="form-control">
                            <option value="TOYOTA">TOYOTA</option>
                            <option value="VW">VW</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                     <label class="col-md-4 control-label" for=""></label>
                    <div class="col-md-4">
                        <input type="submit" value="Pesquisar" class="btn btn-primary" />
                    </div>
                </div>
                

                
            </fieldset>
        </form>
             
        
        <%@include file="/view/includes/scripts.jsp" %>
    </body>
</html>
