<%-- 
    Document   : top10Cliente
    Created on : 24/01/2016, 10:58:30
    Author     : matheus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>

<%-- <c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/" ></c:redirect>
</c:if> --%>

<!DOCTYPE html>
<html>
    <head>

        <title>Estatistica para corrida </title>
        <%@include file="/view/includes/head.jsp" %>
        
        
    </head>
    <body>

        <%@include file="/view/includes/menu.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-md-10 col-xs-10">
                    <h1>Estatistica Corrida</h1>
                </div>
               
            </div>
            <div><h3>TOP 10 Usuario Fidelidade</h3></div>
            <div class="row">
                <div class="col-md-10">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Taxista</th>
                                <th>Usuario</th>
                                <th>Quantidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${top10}" var="d">
                                <tr>
                                    <td>${d.idTaxista}</td>
                                    <td>${d.idUsuario}</td>
                                    <td>${d.quantidadeCorrida}</td>
                                    
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
         <%@include file="/view/includes/scripts.jsp" %>       
        <script type="text/javascript" src="${pageContext.servletContext.contextPath}/assets/js/main.js" ></script>
    </body>
</html>
