$.url  = function (url) {
    return $("base").attr("href") + url;
};


function createMarker(position, map) {
    var marker = L.marker([position.latitude, position.longitude]).addTo(map);
    
    return marker;
}

$(document).ready(function() {

    var map = L.map('my-map').setView([40.02133 , 116.75389], 11);

    var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 20, attribution: osmAttrib});
    map.addLayer(osm);
  
    var myMarkers = [];
   
    $.get($.url("/usuario/pontos"), function (corridasRq) {
        for (var i = 0; i < corridasRq.length; ++i) {
            myMarkers.concat(createMarker(corridasRq[i], map));
        }
        
    });
    
    
});/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


