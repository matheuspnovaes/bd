/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$.url  = function (url) {
    return $("base").attr("href") + url;
};


function createMarker(position, map) {
    var marker = L.marker([position.latitude, position.longitude]).addTo(map);
    
    var message = "<p><strong>Taxi ID: </strong>"+ position.idRota +"</p>";
    message += "<p><strong>Time: </strong>"+ position.observ +"</p>";
    marker.bindPopup(message);

}
function buscarPontos(pt, map){
    var myMarkers = [];

    console.log(pt);
    myMarkers.concat(createMarker(pt, map));
    console.log("I'm here");
    
                
}
function tracarRota(rt,map){
    $.get($.url("/usuario/rota?id="+rt.idRota), function(rota) {
                 console.log("olab");
                var polyline;
                var latlng = [];
                
                for(var j = 0; j < rota.length; j++){
                    buscarPontos(rota[j],map);
                    latlng[j]= L.latLng(rota[j].latitude, rota[j].longitude);
                }
                console.log(latlng);
                polyline= L.polyline(latlng, {color: 'green', opacity: 0.8}).addTo(map);
                console.log(polyline);
             });
}
$(document).ready(function() {

    var map = L.map('my-map').setView([40.02133 , 116.75389], 11);

    var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 20, attribution: osmAttrib});
    map.addLayer(osm);
    map.on('click', function(e) {
      var lati= e.latlng.lat;
      var lon = e.latlng.lng;
     var marker = L.marker([lati, lon]).addTo(map);
     marker.bindPopup("<b>Estou aqui!</b>").openPopup();
    document.getElementById("latitude").value = lati;
    document.getElementById("longitude").value = lon;
    });
   



    $.get($.url("/usuario/buscarRotas"), function (rotas) {
        for (var i = 0; i < rotas.length; ++i) {
            console.log("hello");
            tracarRota(rotas[i],map);
             
        }
        
    });
    
    
});

