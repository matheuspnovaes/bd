function url (url) {
    return $("base").attr("href") + url;
}


$(document).on('click', 'a.link-delete', function(e){
    e.preventDefault();
    
    var $link = $(this);
    
    $.post(url("/taxista/delete?id=" + $link.data("id")), function() {
        $link.parents("tr").remove();
    }); 
});