$.url  = function (url) {
    return $("base").attr("href") + url;
};


function createMarker(position, map) {
    var marker = L.marker([position.latitude, position.longitude]).addTo(map);
    
    return marker;
}

$(document).ready(function() {

    var map = L.map('my-map').setView([40.02133 , 116.75389], 11);

    var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 20, attribution: osmAttrib});
    map.addLayer(osm);
  
    var myMarkers = [];
   
    
    
    map.on('click', function(e) {
      var lati= e.latlng.lat;
      var lon = e.latlng.lng;
     var marker = L.marker([lati, lon]).addTo(map);
     marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
    document.getElementById("latitude").value = lati;
    document.getElementById("longitude").value = lon;
});
});
